#!/usr/bin/env python

import os
from glob import glob
from distutils.core import setup

NAME = 'carddecoders'
VERSION = '0.0'
AUTHOR = 'chrysn'
AUTHOR_EMAIL = 'chrysn@fsfe.org'
WEBSITE = 'http://christian.amsuess.com/tools/carddecoders/'
LICENSE = 'GPL3+ (applications) / LGPL (library parts)'

setup(
        name=NAME,
        version=VERSION,
        author=AUTHOR,
        author_email=AUTHOR_EMAIL,
        url=WEBSITE,

        packages=[
            'carddecoders',
            'carddecoders.en1546',
            'carddecoders.en1546.quick',
            'carddecoders.en1546.quick.guicomponents',
            'carddecoders.future',
            'carddecoders.othercards',
            'carddecoders.iso7816',
            'carddecoders.signatures',
            'nonblocking',
            ],
        scripts=[
            'ecard-cli',
            'ecard-gui',
            'quick-cli',
            'quick-gui',
            'quick-legacyconverter',
            'quick-ntlog',
            'logdecoder',
            ],

        package_data={
            'carddecoders.iso7816': ['sw_codes.txt'],
            'carddecoders.en1546.quick': ['idpsam_addresses.csv', 'gui.xml'],
            },
        )
