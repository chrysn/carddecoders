"""
General purpose ByteString class and MeaningfulByteString for objects that
provide interpretation of ByteStrings.

For the purpose of testing, no assumptions on the nature of
ByteString's __str__ may be made (to get this to fail, let .intiter()
iterate over str(self) instead of self):

>>> ByteString.__str__ = __random_str
""" # this is in the module documentation so that it gets executed early

class ByteString(str): # will derive from (immutable) bytes in python3
    """Byte string that behaves properly in an environment where strings are
    usually represented as space separated hex digits, with some utility
    functions."""

    __slots__ = ()

    def __new__(cls, init=None, bytes=None):
        """"
        ByteString can be initialized from a hex string, an iterable of
        numbers or a (byte) string (by using the `bytes` keyword).

        >>> ByteString("00 23 42")
        ByteString('00 23 42')
        >>> ByteString([0x00, 0x23, 0x42])
        ByteString('00 23 42')
        >>> ByteString(bytes="\\x00\\x23\\x42")
        ByteString('00 23 42')
        >>> len(_)
        3
        """
        if init is None and bytes is None:
            raise ValueError("Either positional argument or bytes has to be set")
        elif init is not None and bytes is not None:
            raise ValueError("Only one of positional argument and bytes can be set")
        elif bytes is None:
            if isinstance(init, ByteString): # have idempotent ByteString(Bytestring(...))
                bytes = init
            elif isinstance(init, basestring): # from hex string
                bytes = init.replace(' ','').replace('\n', '').replace('\t','').decode('hex')
            else: # from list
                bytes = "".join(chr(x) for x in init)

        return super(ByteString, cls).__new__(cls, bytes)

    def intiter(self):
        """Iterate over the integer values of the byte string.

        >>> list(ByteString("00 01 10").intiter())
        [0, 1, 16]
        """
        for c in super(ByteString, self).__str__(): # iterating over super's str should be most safe agains overwritten internals like __iter__
            yield ord(c)

    def __int__(self):
        """
        >>> print "%#02x"%ByteString("fe")
        0xfe
        """
        if len(self) != 1:
            raise ValueError("Can't convert ByteString to int if length is not 1")

        return ord(self)

    def __eq__(self, other):
        """
        >>> ByteString("42") == 0x42
        True
        >>> ByteString(bytes='A') == 'A'
        True
        >>> ByteString('23 42') == ByteString(bytes='\\x23\\x42')
        True
        >>> ByteString('23 42') == [0x23, 0x42]
        True
        >>> ByteString('23 42') == [0x42, 0x23]
        False

        A ByteString does not assume any integer conversion rules unless it's
        all just about a single byte:
        >>> ByteString("00 11") == 0x0011
        Traceback (most recent call last):
            ...
        ValueError: Can not compare ByteString longer than 1 with an integer.
        """
        if isinstance(other, int):
            if len(self) != 1:
                raise ValueError("Can not compare ByteString longer than 1 with an integer.")
            else:
                return ord(self) == other
        elif hasattr(other, '__iter__'):
            if len(self) != len(other):
                return False
            return all(_s == _o for (_s, _o) in zip(self.intiter(), other))
        else:
            return super(ByteString, self).__eq__(other)

    def plainhex(self):
        """Format the ByteString as space separated two digit hex numbers.

        >>> ByteString("fe 80").plainhex()
        'fe 80'
        """

        return " ".join("%02x"%x for x in self.intiter())

    def __repr__(self):
        """
        >>> ByteString("FF")
        ByteString('ff')
        >>> ByteString('00 f0 0f')
        ByteString('00 f0 0f')
        """
        return "%s(%r)"%(type(self).__name__, self.plainhex())

    def decode_bcd(self, use_first_nibble=True, use_last_nibble=True):
        """Treat the base-16 representation like a base-10 one and return as
        integer.

        >>> ByteString('20 10').decode_bcd()
        2010

        In case the numbers are not byte-aligned but start at an odd nibble,
        the first and last nibble can be disabled:
        >>> ByteString('f1 23').decode_bcd(use_first_nibble=False)
        123
        >>> ByteString('f1 23 4e').decode_bcd(False, False)
        1234

        Of course, this can raise exceptions if the data does not look like
        BCD:
        >>> ByteString('12 3f').decode_bcd()
        Traceback (most recent call last):
            ...
        ValueError: Not a BCD string.
        """
        string = "".join("%02x"%x for x in self.intiter())
        sliced = string[None if use_first_nibble else 1:None if use_last_nibble else -1]
        try:
            return int(sliced)
        except ValueError:
            raise ValueError("Not a BCD string.")

    def int_bigendian(self):
        """Convert to integer assuming big endian-ness (first byte is most
        significant).

        >>> ByteString('f1 20').int_bigendian() == 0xf1 * 256 + 0x20
        True
        """
        return sum(value << (8*exp) for (exp, value) in enumerate(reversed(list(self.intiter()))))

    def __getitem__(self, key):
        """
        >>> ByteString("11 22 33 44 55 66")[-1]
        ByteString('66')
        >>> ByteString("ba ad be ef")[1:3]
        ByteString('ad be')
        """

        sliced = super(ByteString, self).__getitem__(key)

        # this explicitly returns ByteString and not type(self) because
        # subtypes are likely to be specialized on working on a particular kind
        # of record, and their properties will be lost when slicing
        return ByteString(bytes=sliced)

    def __getslice__(self, i, j): # overwrite the __getslice__ method str provides
        return self.__getitem__(slice(i, j))

    def __getnewargs__(self):
        """Be picklable even with newer protocols

        >>> import pickle
        >>> pickle.loads(pickle.dumps(ByteString("01 23 45 67 89"), 2.0))
        ByteString('01 23 45 67 89')
        """
        return (self.plainhex(), )

class MeaningfulByteString(ByteString):
    """Base clas for ByteString interpretation classes.

    Build those classes like this:
    >>> class Vector3(MeaningfulByteString):
    ...     LENGTH = 12
    ...     x = property(lambda self: self[0:4].int_bigendian())
    ...     y = property(lambda self: self[4:8].int_bigendian())
    ...     z = property(lambda self: self[8:12].int_bigendian())
    >>> v = Vector3("00 00 00 01   00 00 01 00    00 00 00 00") # spaces inserted for readability
    >>> v.x
    1
    >>> v.y
    256

    Giving an optional LENGTH class member for length checking:
    >>> v = Vector3("00 00 00 00   00")
    Traceback (most recent call last):
        ...
    ValueError: Bad length.
    """

    def __init__(self, stringself):
        if hasattr(self, 'LENGTH') and self.LENGTH != len(self):
            raise ValueError("Bad length. (Expected %d, got %d)"%(self.LENGTH, len(self)))
        super(MeaningfulByteString, self).__init__()

    def explain(self):
        """Format the string in a human readable way

        >>> class Vector3(MeaningfulByteString):
        ...     LENGTH = 12
        ...     x = property(lambda self: self[0:4].int_bigendian())
        ...     y = property(lambda self: self[4:8].int_bigendian())
        ...     z = property(lambda self: self[8:12].int_bigendian())
        >>> v = Vector3("00 00 00 01   00 00 01 00    00 00 00 10") # spaces inserted for readability
        >>> print v.explain()
        <00 00 00 01 00 00 01 00 00 00 00 10>: x=1, y=256, z=16
        """
        values = []
        for attr in sorted(dir(self)):
            if attr.startswith('_') or attr == 'LENGTH':
                continue
            value = getattr(self, attr)
            if callable(value):
                continue
            values.append("%s=%s"%(attr, getattr(self, attr)))
        return "<%s>: %s"%(self.plainhex(), ", ".join(values))

def caching_property(getter, doc=None):
    """Replacement for property (without set/delete) that caches the results,
    useful for MeaningfulByteString getters.

    Does not take any precautions against changes in the object, so best use
    with otherwise immutable objects."""

    propname = '__cache_%s'%hash(getter)
    def wrapped_getter(self):
        if not hasattr(self, propname):
            result = getter(self)
            setattr(self, propname, result)
        return getattr(self, propname)
    return property(wrapped_getter, doc=doc)


def __random_str(self): # function needed in doc tests. can't be defined in the doc test for some scoping reasons
    import random
    return "".join(chr(random.randint(0, 255)) for i in [0]*random.randint(0, 10))
