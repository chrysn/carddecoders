"""
GTK widgets that are used in one or more carddecoder.

This is only a temporary solution -- this stuff should either go up into pygtk
or form an own independent module in the style of sexy.
"""

import gtk

class CategoryDefinitionWidget(gtk.Table):
    """Widget that displays a list of items grouped to categories in the style
    of the "Frames and Separators" chapter of the Gnome HIG.

    `items` is an iterable containing (catgory_label, label, data) triples.
    The latter two can be strings or widgets. Items will be displayed in the
    order in which they are given, items with equal category labels will be
    grouped under their caption.

    A CategoryDefinitionWidget(
        [
            ("some category", "What:", "Spam"),
            ("some category", "How much:", "very much"),
            ("another category", gtk.Label("Widget:"), gtk.Entry()),
            ]
    will look like this:

        *some category*
            What:      Spam
            How much:  very much
        *another category*
            Widget:    [Text field]
    """
    __gtype_name__ = "CategoryDefinitionWidget"

    def __init__(self, items=None):
        super(CategoryDefinitionWidget, self).__init__(columns=2)

        self.props.border_width = 6 # concerning paddings: the 6 pixels spread through the layouting should add up to the 12px left, 12px between, and 12px right of each column

        if items:
            self.set_items(items)

    def set_items(self, items):
        for c in self.get_children():
            self.remove(c)

        last_category = None
        row = 0
        for category_label, label, data in items:
            # category separator
            if category_label != last_category:
                clabel = gtk.Label()
                clabel.set_markup('<b>%s</b>'%category_label) # FIXME: escape?
                clabel.props.xalign = 0
                self.attach(clabel, 0, 2, row, row+1, yoptions=gtk.FILL, xpadding=6, ypadding=4)
                row += 1
                last_category = category_label

            # conversion to widgets if required
            if not isinstance(label, gtk.Widget):
                label = gtk.Label(label)
                label.props.xalign = 0
            if not isinstance(data, gtk.Widget):
                data = gtk.Label(data)
                data.props.xalign = 0

            indent = gtk.Alignment()
            indent.set_padding(0, 0, 12, 0)
            indent.add(label)

            # adding to grid
            self.attach(indent, 0, 1, row, row+1, xoptions=gtk.FILL, yoptions=gtk.FILL, xpadding=6, ypadding=1)
            self.attach(data, 1, 2, row, row+1, yoptions=gtk.FILL, xpadding=6, ypadding=2)

            row += 1

        self.show_all()

class AboutDialogWithMarkup(gtk.AboutDialog):
    """An AboutDialog that has an additional function to set its comment text with markup."""

    __gtype_name__ = "AboutDialogWithMarkup"

    def set_comments_markup(self, text):
        """Call set_markup on the comment label with the text."""

        # FIXME: this is an ugly hack.
        PLACEHOLDER = 'PLACEHOLDER'
        self.props.comments = PLACEHOLDER
        comment_label = self.get_children()[0].get_children()[0].get_children()[2]
        assert comment_label.props.label == PLACEHOLDER, "Structure of about dialog has changed."

        comment_label.set_markup(text)

def safecb(function):
    """Wrap a function in a way that they can be used as callbacks whose
    exceptions are shown to the user and behave like a proper exception."""

    def wrapped(*args, **kwargs):
        try:
            value = function(*args, **kwargs)
        except: # should be except Exception( as/,) e: -- unfortunately, pyscard's exceptions are not derived from Exception
            # didn't get this to work; exceptions in cell display functions always create multiple error messages. turning this into a noop until this is solved. FIXME.
            #e_type, e_value, e_traceback = sys.exc_info()
            #d = gtk.MessageDialog(type=gtk.MESSAGE_ERROR, buttons=gtk.BUTTONS_OK)
            #d.props.text = unicode(e_value)
            #d.run()
            #d.destroy()
            #gtk.main_quit()
            raise
        else:
            return value

    wrapped.__name__ = function.__name__
    wrapped.__doc__ = function.__doc__
    return wrapped
