# encoding: utf-8
"""Objects representing currencies and amounts of money in a given currency.

Typically created from en1546 money values."""

try:
    from collections import namedtuple
except ImportError:
    from .future import namedtuple

class Currency(int):
    """Currency object for ISO 4217 currencies

    >>> EUR = Currency(978)
    >>> EUR.symbol
    u'\\u20ac'
    """
    # constants from http://en.wikipedia.org/wiki/ISO_4217
    SYMBOLS = {978: u'€', 40: u'ÖS'}
    CODES = {978: u'EUR', 40: 'ATS'}
    EXPONENTS = {978: 2, 40: 2}

    number = property(lambda self: int(self))
    symbol = property(lambda self: self.SYMBOLS[self])
    code = property(lambda self: self.CODES[self])
    exponent = property(lambda self: self.EXPONENTS[self])
    known = property(lambda self: self in self.SYMBOLS and self in self.CODES and self in self.EXPONENTS)

    def __repr__(self):
        return '<%s %d (%s)>'%(type(self).__name__, self.number, self.code if self.known else 'unknown')

class Money(namedtuple('Money', 'value, currency')):
    """Money of any currency as specified at construction time.

    >>> EUR = Currency(978)
    >>> price = Money(140, EUR) # "140 of the small units of EUR"
    >>> print unicode(price).encode('utf8')
    € 1.40
    """
    def __unicode__(self):
        # FIXME: use locales?
        if self.currency.known:
            factor = 10**self.currency.exponent
            return u'%s %d.%02d'%((self.currency.symbol,)+divmod(self.value, factor))
        else:
            return u'"%d" %d'%(self.currency.number, self.value)

    def __repr__(self):
        if self.currency.known:
            return '<%s %de-%d>'%(self.currency.code, self.value, self.currency.exponent)
        else:
            return '<%d of unknown currency %d>'%(self.value, self.currency.number)
