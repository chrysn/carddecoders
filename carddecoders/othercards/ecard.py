"""Interpretation of Austrian "ecard" type cards."""

import datetime

from ..bytestring import MeaningfulByteString
from ..bytestring import ByteString as B

from .. import signatures

class Date(MeaningfulByteString):
    """Date format used on ecards"""

    LENGTH = 15

    def __init__(self, otherself):
        if not self[8:] == '120000Z':
            raise Exception("Unknown data after date.")

    year = property(lambda self: int(self[:4].decode('ascii')))
    month = property(lambda self: int(self[4:6].decode('ascii')))
    day = property(lambda self: int(self[6:8].decode('ascii')))

    date = property(lambda self: datetime.date(self.year, self.month, self.day))

class Data1(MeaningfulByteString):
    LENGTH = 12

    card_id = property(lambda self: self[2:].decode_bcd())

    unparsed = property(lambda self: (self[:2], )) # or static

class LengthPrefixedZeroSplitData(MeaningfulByteString):
    """Most files on the ecard seem to have their bodies organized as a
    key/value pair; this sets the basic infrastructure for parsing such
    files."""
    def fill_fields(self, remaining):
        self.fields = []

        while True:
            length = int(remaining[0])
            self.fields.append(self.Field(remaining[1:length+1]))

            if len(remaining) > length+1:
                if remaining[length+1] == 0: # rest should also be null
                    if any(remaining[length+1:].intiter()):
                        raise Exception("Expecting rest of entry to be empty")
                    break
                if not remaining[length+1] == '0': # funny NULL termination
                    raise Exception("Field not '0'-terminated.")
            else:
                break

            remaining = remaining[length+2:]

    class Field(MeaningfulByteString):
        def __init__(self, otherself):
            if not self[0] == 0x06:
                raise Exception("Unexpected first byte in field")

            keylength = int(self[1])
            self.key = self[2:2+keylength]

            if not self[2+keylength] == 0x31:
                raise Exception("Unexpected byte after key")

            rest = self[3+keylength:]
            if int(rest[0]) != len(rest)-1:
                raise Exception("Unexpected rest length")
            if int(rest[2]) != len(rest)-3:
                raise Exception("Unexpected rest length indicator 2")
            self.data_start = rest[1] # meaning is unknown
            self.data = rest[3:]

class Data2(LengthPrefixedZeroSplitData):
    # LENGTH = 256 # why check for length in a variable length record

    def __init__(self, otherself):
        if not self[0] == '0':
            raise Exception("Unexpected data format as first byte")
        if self[3] == '0':
            self.prefix = self[1:3]

            self.fill_fields(self[4:])
        else:
            self.prefix = self[1:2]
            if not self[2] == '0':
                raise Exception("Unexpected data format as fourth byte")

            self.fill_fields(self[3:])

        self.insurance_number = self.card_number = self.name_first = self.name_last = self.birth = self.sex = self.title_before_name = None # defaults in case a field is not present

        for f in self.fields:
            if f.key == B('2a 28 00 0a 01 04 01 01'):
                self.insurance_number = f.data.decode('ascii')
            elif f.key == B('2a 28 00 0a 01 04 01 03'):
                self.card_number = int(f.data)
            elif f.key == B('55 04 2a'):
                self.name_first = f.data.decode('utf8')
            elif f.key == B('55 04 04'):
                self.name_last = f.data.decode('utf8')
            elif f.key == B('2b 06 01 05 05 07 09 01'):
                self.birth = Date(f.data).date
            elif f.key == B('2b 06 01 05 05 07 09 03'):
                self.sex = f.data.decode('ascii')
            elif f.key == B('55 04 0c'):
                self.title_before_name = f.data.decode('utf8')
            else:
                raise Exception("Unknown field %r, data %r (%r)"%(f.key, f.data, str(f.data)))


class Data3(LengthPrefixedZeroSplitData):
    # LENGTH = 126 # why check for length in a variable length record

    def __init__(self, otherself):
        print repr(self)
        if not self[0] == '0':
            raise Exception("Unexpected data format as first byte")
        if self[2] == '0':
            self.prefix = self[1]
            self.fill_fields(self[3:])
        elif self[3] == '0':
            self.prefix = self[1:2]
            self.fill_fields(self[4:])
        else:
            raise Exception("Unexpected data format as third/fourth byte")

        self.organization = self.organization_id = self.card_id = self.expiry = self.country = self.name = self.name_first = self.birth = self.insurance_number = None # defaults in case a field is not present

        for f in self.fields:
            if f.key == B('2a 28 00 0a 01 04 01 14'):
                self.organization = f.data.decode('utf8')
            elif f.key == B('2a 28 00 0a 01 04 01 15'):
                self.organization_id = f.data.decode('ascii')
            elif f.key == B('2a 28 00 0a 01 04 01 16'):
                self.card_id = int(f.data.decode('ascii'))
            elif f.key == B('2a 28 00 0a 01 04 01 17'):
                self.expiry = Date(f.data).date
            elif f.key == B('55 04 06'):
                self.country = f.data.decode('ascii')
            elif f.key == B('2a 28 00 0a 01 04 01 19'):
                self.name = f.data.decode('utf8')
            elif f.key == B('2a 28 00 0a 01 04 01 1a'):
                self.name_first = f.data.decode('utf8')
            elif f.key == B('2b 06 01 05 05 07 09 01'):
                self.birth = Date(f.data).date
            elif f.key == B('2a 28 00 0a 01 04 01 1b'):
                self.insurance_number = f.data.decode('ascii')
            else:
                raise Exception("Unknown field %r, data %r (%r)"%(f.key, f.data, str(f.data)))

class ECard(object):
    """Austrian "ecard" (social insurance id card)"""

    signature = signatures.SmartcardSignature([
        '3B BD 18 00 81 31 FE 45 80 51 02 67 05 18 B1 02 02 02 01 81 05 31', # not sure about the ..31 card; never seen one yet but smartcard_list.txt claims it's ecard as well
        '3B BD 18 00 81 31 FE 45 80 51 02 67 04 14 B1 01 01 02 00 81 05 3D',
        '3B DD 96 FF 81 B1 FE 45 1F 03 80 31 B0 52 02 03 64 04 1B B4 22 81 05 18', # unknown to lrousseau; seems to work differently
        ], ["Austrian ecard"])

    def __init__(self, connection):
        # c.check_transmit(B('00 a4 00 0c 02 3f 00')) # seen, but not really required
        atr = connection.getATR()

        if not self.signature.match_atr(atr):
            raise Exception("Card seems not to be an ecard.") # FIXME exception type

        self._data1 = Data1(connection.readBinary(short_ef=2))

        connection.check_transmit(B('00 a4 04 0c 08 d0 40 00 00 17 01 01 01'))
        self._data2 = Data2(connection.readBinary(short_ef=1))
        self._data3 = Data3(connection.readBinary(short_ef=2))

    @property
    def card_id(self):
        assert self._data1.card_id == self._data3.card_id, "Card IDs vary"
        return self._data1.card_id

    insurance_number = property(lambda self: self._data2.insurance_number)
    title_before_name = property(lambda self: self._data2.title_before_name)
    name_first = property(lambda self: self._data2.name_first)
    name_last = property(lambda self: self._data2.name_last)
    birth = property(lambda self: self._data2.birth)
    expiry = property(lambda self: self._data3.expiry)
    organization_id = property(lambda self: self._data3.organization_id)
    organization = property(lambda self: self._data3.organization)
    sex = property(lambda self: self._data2.sex)
    card_number = property(lambda self: self._data2.card_number, doc="Number of the card instance, starting at 1")
    country = property(lambda self: self._data3.country)
    # FIXME: birth, insurance_number and name/name_first (name seems to include title) are encoded in field 3 as well on new cards.

    complete_name = property(lambda self: " ".join(filter(lambda x:x, [self.title_before_name, self.name_first, self.name_last])))
