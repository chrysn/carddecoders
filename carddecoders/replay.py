import itertools

from .iso7816.smartcard import BaseConnection
from .conversation import Request, Reset

class ReplayError(Exception):
    """Raised when a request can not be satisfied from a conversation."""

class ReplayConnection(BaseConnection):
    """Mimick a `smartcard.Connection` using a `Conversation` for replaying

    This is just a base class; use NaiveReplayConnection,
    StrictReplayConnection or implement your own card logic."""

    def __init__(self, conversation):
        self.conversation = conversation

class StrictReplayConnection(ReplayConnection):
    """Mimick a `smartcard.Connection` using a `Conversation` for replaying

    This is a strict implementation: It requires the conversation to start with
    a reset, will jump there after each getATR and will only take the very
    command issued next.
    """

    def __init__(self, conversation):
        # FIXME upstream: please use new style classes
        #super(StrictReplayConnection, self).__init__(conversation)
        ReplayConnection.__init__(self, conversation)

        self.line = None

    def getATR(self):
        if not isinstance(self.conversation[0], Reset):
            self.line = None
            raise ReplayError("Conversation has to start with a `Reset`.")
        self.line = 1
        return self.conversation[0].answer

    def transmit(self, request):
        if self.line is None:
            raise ReplayError("Replay has to start with getATR.")
        if self.line >= len(self.conversation):
            raise ReplayError("Conversation exhausted.")
        currentline = self.conversation[self.line]
        if list(currentline.request.intiter()) != list(request.intiter()):
            self.line = None
            raise ReplayError("Conversation had different next transmission.")
        self.line += 1
        return (currentline.response, currentline.sw)


class NaiveReplayConnection(ReplayConnection):
    """Mimick a `smartcard.Connection` using a `Conversation` for replaying

    This is a very naive implementation: It will keep a "current" line and for
    each command look for it starting in the current line, then retry from the top.

    THIS CAN EASILY PRODUCE WRONG RESULTS!

    An example might be an app that in an earlier version worked like this:

    1. Select DF 01
    2. Select EF 01
    3. Select DF 02
    4. Select EF 01
    5. Select EF 02

    When it changes its behavior to

    1. Select DF 01
    2. Select EF 01
    3. Select EF 02
    4. ...

    this replayer will, at command 3, skip over the old commands 3 and 4 and
    return the result for 02/02 instead of complaining that 01/02 was not found
    in the replay.
    """

    def __init__(self, conversation):
        # FIXME upstream: please use new style classes
        #super(NaiveReplayConnection, self).__init__(conversation)
        ReplayConnection.__init__(self, conversation)

        self.line = 0

    def getATR(self):
        self.line = 0
        for line_number, line  in enumerate(self.conversation):
            if isinstance(line, Reset):
                self.line = line_number
                return line.answer
        raise ReplayError("No ATR found.")

    def transmit(self, request):
        after_line = ((self.line+offset, line) for (offset, line) in enumerate(self.conversation[self.line:]))
        before_line = enumerate(self.conversation[:self.line])

        for line_no, line in itertools.chain(after_line, before_line):
            if isinstance(line, Request) and list(line.request.intiter()) == list(request.intiter()):
                self.line = line_no + 1
                return line.response, line.sw
        raise ReplayError("No matching line found.")
