"""Tools to iterate over all DF/EF combinations that look like those used on
the Quick cards and store the results.

Ihave no idea if this will work on any card but the Quick card."""

# currently unused and non-functional
'''
import os
import datetime
import zipfile
from . import sw

class CardWalker(object):
    """Baseic file system walker. Use .walk() to start the iteration; this will
    trigger the received_atr/found_df/found_ef/classified_ef methods as soon as
    data arrives."""
    def __init__(self, card):
        self.card = card

    def walk(self):
        self.received_atr(self.card.getATR())

        for (df1, df2, result) in self._search_dfs():
            self.found_df(df1, df2, result)

            if not isinstance(result, Exception):
                for (ef1, ef2, result) in self._search_efs():
                    self.found_ef(df1, df2, ef1, ef2, result)

                    if not isinstance(result, Exception):
                        data = self._classify_current_ef()
                        self.classified_ef(df1, df2, ef1, ef2, data)

    # hook in here

    def received_atr(self, atr):
        pass

    def found_df(self, df1, df2, data):
        """data is exception or data returned on SelectFile"""

    def found_ef(self, df1, df2, ef1, ef2, data):
        """data is exception or data returned on SelectFile"""

    def classified_ef(self, df1, df2, ef1, ef2, data):
        """data is either
        * a BinaryString (if ReadBinary worked),
        * a list of records, each BinaryData result or exception
        * a CardError rexception
        """

    # internal walking code

    def _search_dfs(self):
        dfs = [(0x3f, 0x00)] + [(0xdf, i) for i in range(256)]
        for df1, df2 in dfs:
            try:
                on_select = self.card.selectFile(df1, df2)
            except sw.CardException, e:
                yield (df1, df2, e)
            else:
                yield (df1, df2, on_select)

    def _search_efs(self):
        for ef1 in range(256):
            if ef1 in (0xdf, 0x3f):
                # would address a df or mf, not an ef
                continue

            for ef2 in range(256):
                try:
                    on_select = self.card.selectFile(ef1, ef2)
                except sw.CardException, e:
                    yield (ef1, ef2, e)
                else:
                    yield (ef1, ef2, on_select)

    def _classify_current_ef(self):
        try:
            try:
                return self.card.readBinary()
            except (sw.CardException6981, ): # command incompatible with file structure
                records = []
                for record_id in range(0, 256):
                    try:
                        record_data = self.card.readRecord(record_id)
                    except sw.CardException, e:
                        records.append(e)
                    else:
                    records.append(record_data)
                return records
        except sw.CardException, e:
            return e


class ParsError(ValueError):
    """Raised when a card description file does not look like it should."""

class StringPlaceholder(object):
    """Object stored in the CardIndex that represents a BinaryString of known
    length but unknown contents"""

    def __init__(self, length):
        self.length = length

    def __repr__(self):
        return '<StringPlaceholder length %d>'%self.length

class CardIndex(object):
    """Object to represent the table of contents of a card."""
    def __init__(self):
        self.atr = None
        self.dfs = {}
        self.efs = {}
        self.ef_classifications = {}

    @property
    def empty(self):
        return not any(self.atr, self.dfs, self.efs, self.ef_classifications)

    def parse(self, data):
        if not self.empty:
            raise Exception("Won't parse into non-empty index")

        current_df = None
        current_ef = None

        def parse_select_result(s):
            if s == ': available':
                return StringPlaceholder(0)
            elif l[8:].startswith(": available, "):
                on_select_length = int(l[21:l.index(' byte')])
                if l[l.index(' byte'):] not in (' bytes on select', ' byte on select'):
                    raise ParsError('Invalid select result line.')
                return StringPlaceholder(on_select_length)
            elif l[8:].startswith(': unavailable ('):
                sw1 = int(l[23:25], 16)
                sw2 = int(l[26:28], 16)
                return create_exception((sw1, sw2))
            else:
                raise ParsError("Unknown select result.")

        def parse_contents(s):
            if contents.startswith('binary content ('):
                content_length = int(contents[16:contents.index(' byte')])
                if contents[contents.index(' byte'):] not in (' byte)', ' bytes)'):
                    raise ParsError('Invalid binary content line')
                return StringPlaceholder(content_length)
            elif contents.startswith('record'):
                if contents.startswith('records '):
                    record_id_start = int(contents[7:contents.index(' - ')])
                    record_id_end = int(contents[contents.index(' - ')+3:contents.index(': ')])
                    record_id = range(record_id_start, record_id_end+1)
                elif contents.startswith('record '):
                    record_id = int(contents[7:contents.index(': ')])
                else:
                    raise ParsError('Looks like a record entry, but is none.')
                record_data = contents[contents.index(': ')+2:]
                if ' byte' in record_data:
                    record_length = int(record_data[:record_data.index(' byte')])
                    if record_data[record_data.index(' byte')] not in (' byte', ' bytes'):
                        raise ParsError("Invalid record data line")
                    return (record_id, record_length)
                elif record_data.startswith('unavailable ('):
                    sw1 = int(record_data[13:15], 16)
                    sw2 = int(record_data[16:18], 16)
                    return (record_id, create_exception((sw1, sw2)))
            elif contents.startswith('unreadable ('):
                sw1 = int(l[12:24], 16)
                sw2 = int(l[15:17], 16)
                return create_exception((sw1, sw2))


        for l in data.split('\n'):
            if l == '':
                pass # empty lines are ok

            elif l.startswith('ATR length: '):
                self._atr_length = int(l[12:])

            elif l.startswith('DF '):
                current_ef = None
                if ' - ' in l[3:l.index(': ')]: # FIXME: make sure unparser never ever bundles 3F00 with DF01
                    current_df = None
                    current_df_range_start = int(l[3:5], 16) * 256 + int(l[6:8], 16))
                    current_df_range_end = int(l[11:13], 16) * 256 + int(l[14:16], 16))
                    current_df_range = [(_df//256, _df%256) for _df in range(current_df_range_start, current_df_range_end+1)]
                else:
                    current_df = (int(l[3:5], 16), int(l[6:8], 16))

                df_description = parse_select_result(l[l.index(': ')+2:])

                for df in current_df_range:
                    if df in self.dfs:
                        raise ParsError("Duplicate DF")
                    self.dfs[df] = df_description

            elif l.startswith('    EF '):
                if not current_df:
                    raise ParsError('No DF to store EF in')
                if ' - ' in l[7:l.index(': ')]:
                    current_ef = None
                    current_ef_range_start = int(l[7:9], 16) * 256 + int(l[10:12], 16))
                    current_ef_range_end = int(l[15:17], 16) * 256 + int(l[18:20], 16))
                    current_ef_range = [(_ef//256, _ef%256) for _ef in range(current_ef_range_start, current_ef_range_end+1)]
                else:
                    current_ef = (int(l[7:9], 16), int(l[10:12], 16))
                    current_ef_range = [current_ef]

                ef_description = parse_select_result(l[l.index(': ')+2:])

                for ef in current_ef_range:
                    if (current_df + ef) in self.dfs:
                        raise ParsError("Duplicate EF")
                    self.dfs[current_df + ef] = ef_description

            elif l.startswith('        '):
                if not current_ef:
                    raise ParsError('No EF to store description in')
                data = parse_contents(l[8:])

                if isinstance(data, tuple):
                    for record_id in data[0]:
                        record_list = self.ef_classifications.get(current_df + current_ef, [])
                        if len(record_list) != record_id:
                            raise ParseError("Record IDs have to be ascending from 0")
                        record_list.append(data[1])
                else:
                    if (current_df + current_ef) in self.ef_classifications:
                        raise ParsError("Duplicate EF classification")
                    self.ef_classifications[current_df + current_ef] = data

    def unparse(self):
        result = []
        result.append('ATR length: %d'%len(self.atr))
        result.append('')

        last_df_description = None
        last_df_start = None
        for (df1, df2), df_data in sorted(self.dfs.items()):
            if isinstance(df_data, Exception):
                df_description = 'unavailable (%02X %02X -- %r)'%(df_data.sw[0], df_data.sw[1], df_data.text)
            else:
                if df_data:
                    df_description = 'available, %d byte%s on select'%(len(df_data), '' if len(df_data) == 1 else 's')
                else:
                    df_description = 'available'

            # bundle dfs
            if df_description == last_df_description and isinstance(df_data, Exception): # don't bundle working dfs, there are few enough of them and they contain efs
                result.pop()
                result.append('DF %02X %02X - %02X %02X: %s'%(last_df_start[0], last_df_start[1], df1, df2, df_description))
            else:
                result.append('DF %02X %02X: %s'%(df1, df2, df_description))
                last_df_start = (df1, df2)
                last_df_description = df_description

            # list efs
            last_ef_description = None
            last_ef_start = None
            for (_df1, _df2, ef1, ef2), ef_data in sorted(self.efs.items()):
                if (_df1, _df2) != (df1, df2):
                    # does not apply here
                    continue

                if isinstance(df_data, Exception):
                    raise Exception("Last DF could not be opened, but EF data. Can't represent that. (DF might have been bundled.)")

                if isinstance(ef_data, Exception):
                    ef_description = 'unavailable (%02X %02X -- %r)'%(ef_data.sw[0], ef_data.sw[1], ef_data.text)
                else:
                    if ef_data:
                        ef_description = 'available, %d byte%s on select'%(len(ef_data), '' if len(ef_data) == 1 else 's')
                    else:
                        ef_description = 'available'

                # bundle efs
                if ef_description == last_ef_description and isinstance(ef_data, Exception): # don't bundle working efs, there are few enough of them and they contain more data
                    result.pop()
                    result.append('    EF %02X %02X - %02X %02X: %s'%(last_ef_start[0], last_ef_start[1], ef1, ef2, ef_description))
                else:
                    result.append('    EF %02X %02X: %s'%(ef1, ef2, ef_description))
                    last_ef_start = (ef1, ef2)
                    last_ef_description = ef_description

                # describe efs
                if (df1, df2, ef1, ef2) in self.ef_classifications:
                    if isinstance(ef_data, Exception):
                        raise Exception("Last EF could not be opened, but EF description. Can't represent that. (EF might have been bundled.)")

                    efc_data = self.ef_classifications((df1, df2, ef1, ef2))

                    if isinstance(efc_data, Exception):
                        result.append('        unreadable (%02X %02X -- %r)'%(efc_data.sw[0], efc_data.sw[1], efc_data.text))
                    elif isinstance(efc_data, BinaryString):
                        result.append('        binary content (%d byte%s)'%(len(efc_data), '' if len(efc_data) == 1 else 's'))
                    elif isinstance(efc_data, list):
                        last_record_start = None
                        last_record_description = None
                        for (record_id, record_data) in enumerate(efc_data):
                            if isinstance(record_data, Exception):
                                record_description = 'unavailable (%02X %02X -- %r)'%(record_data.sw[0], record_data.sw[1], record_data.text)
                            else:
                                record_description = '%d byte%s'%(len(record_data), '' if len(record_data) == 1 else 's')

                            # bundle records
                            if record_description == last_record_description:
                                result.pop()
                                result.append('        records %d - %d: %s'%(last_record_start, record_id, record_description))
                            else:
                                result.append('        record %d: %s'%(record_id, record_description))
                                last_record_start = record_id
                                last_record_description = record_description

                    else:
                        raise AssertionError()


        return "\n".join(result) + '\n'

class LoggingCardWalker(CardWalker):
    """CardWalker that stores all results in an index for later access."""
    def __init__(self, *args, **kwargs):
        super(LoggingCardWalker, self).__init__(*args, **kwargs)

        self.index = CardIndex()

        self.index_complete = False

    def received_atr(self, atr):
        super(LoggingCardWalker, self).received_atr(atr)
        self.index.atr = atr
    def found_df(self, df1, df2, data):
        super(LoggingCardWalker, self).found_df(df1, df2, data)
        self.index.dfs[(df1, df2)] = data
    def found_ef(self, df1, df2, ef1, ef2, data):
        super(LoggingCardWalker, self).found_ef(df1, df2, ef1, ef2, data)
        self.index.efs[(df1, df2, ef1, ef2)] = data
    def classified_ef(self, df1, df2, ef1, ef2, data):
        super(LoggingCardWalker, self).classified_ef(df1, df2, ef1, ef2, data)
        self.index.ef_classifications[(df1, df2, ef1, ef2)] = data

    def walk(self):
        super(LoggingCardWalker, self).walk()

        self.index_complete = True

    def _check_ready(self):
        if not self.index_complete:
            raise Exception("This LoggingCardWalker has to to be .walk()ed before logs can be retrieved.")

    @property
    def atr(self):
        self._check_ready()
        return self.index.atr
    @property
    def dfs(self):
        self._check_ready()
        return self.index.dfs
    @property
    def efs(self):
        self._check_ready()
        return self.index.efs
    @property
    def ef_classifications(self):
        self._check_ready()
        return self.index.ef_classifications

class CardDumpGenerator(CardDescriptionGenerator):
    def __init__(self, card, zipfile_name):
        """Write a file at path contining info"""

        super(CardDumpGenerator, self).__init__(card)

        if os.path.exists(zipfile_name): # not required any more in python2.6
            self.dump_file = zipfile.ZipFile(zipfile_name, 'a')
        else:
            self.dump_file = zipfile.ZipFile(zipfile_name, 'w')

        self.timestamp = datetime.datetime.utcnow().strftime('%Y-%m-%d-%H-%M-%S')

    def store(self, path, info):
        self.dump_file.writestr(self.timestamp + '/' + path, info)

    def walk(self):
        try:
            super(CardDumpGenerator, self).walk()
        except KeyboardInterrupt:
            self.store('index.partial', self.index.unparse())
            raise
        else:
            self.store('index', self.index.unparse())
        finally:
            self.dump_file.close()

    def received_atr(self, atr):
        super(CardDumpGenerator, self).received_atr(atr)

        self.store('atr', atr)

    def found_df(self, df1, df2, data):
        super(CardDumpGenerator, self).found_df(df1, df2, data)

        if not isinstance(data, Exception) and data:
            self.store('%02X%02X/select_response'%(df1, df2), data)

    def found_ef(self, df1, df2, ef1, ef2, data):
        super(CardDumpGenerator, self).found_ef(df1, df2, ef1, ef2, data)

        if not isinstance(data, Exception) and data:
            self.store('%02X%02X/%02X%02X/select_response'%(df1, df2, ef1, ef2), data)

    def classified_ef(self, df1, df2, ef1, ef2, data):
        super(CardDumpGenerator, self).classified_ef(df1, df2, ef1, ef2, state, data)

        if isinstance(data, BinaryString):
            self.store('%02X%02X/%02X%02X/binary'%(df1, df2, ef1, ef2), data)
        elif isinstance(data, list):
            for (record_id, record_data) in enumerate(data):
                if not isinstance(record_data, Exception):
                    self.store('%02X%02X/%02X%02X/record_%02X'%(df1, df2, ef1, ef2, record_id), record_data)
'''
