"""Interpretation of (SW1, SW2) return codes"""

import pkgutil
import re

from ..bytestring import ByteString as B


SW_OK = B('90 00')


# actually, i'd like the sw responses to stay what they are (ByteString) and
# still be raise-able. everything that can be raised must be derived from
# Exception, and things that are derived from Exception can't be also derived
# from str since both are implemented in C.
# so, instaed of
#class CardException(MeaningfulBytestring, Exception):
#    __slots__ = ()
#    message = ...
#    def __repr__(self):
#        ...
# we have
class CardException(Exception):
    def __init__(self, sw):
        self.__dict__['_sw'] = sw

    def __getattr__(self, attr):
        return getattr(self._sw, attr)
    def __setattr__(self, attr, value):
        setattr(self._sw, attr, value)

    message = 'Unknown return code' # will be overwritten by child classes

    def __repr__(self):
        return '<%s: %s (%s)>'%(type(self).__name__, self.message, self.plainhex())

    def __str__(self):
        return '%s: %s'%(self.plainhex(), self.message)

    def __reduce__(self):
        # picklability is important in the context of multiprocessing
        return (create_sw, (self.__dict__['_sw'], ))

# a special case to replace CardException9000. behaves quite like the other
# exceptions, but is none
class CardNonexception(B):
    message = 'OK'

def load_sw_codes():
    lines = (line.split(None, 2) for line in pkgutil.get_data('carddecoders.iso7816.sw', 'sw_codes.txt').split('\n') if line and not line.startswith('#'))
    parsed = [('%s %s'%(sw1, sw2), text.strip()) for (sw1, sw2, text) in lines]

    exceptions = [type(
        'CardException%s'%sw.replace(' ', '').upper(),
        (CardException, ),
        {
            '__slots__':(),
            'message': "%s: %s"%(sw, message) if 'x' in message else message
        }) if sw != SW_OK.plainhex() else CardNonexception
        for (sw, message) in parsed]

    exacts = dict((B(sw), exc) for ((sw, message), exc) in zip(parsed, exceptions) if 'x' not in sw)
    fuzzy = [(re.compile(sw.replace('x', '.')), exc) for ((sw, message), exc) in zip(parsed, exceptions) if 'x' in sw]

    return exceptions, exacts, fuzzy

exceptions, exact_codes, fuzzy_codes = load_sw_codes()

def _sw_match(sw):
    """Find a matching exception class for a given sw 2-tuple"""
    if sw in exact_codes:
        return exact_codes[sw]
    else:
        plainhex = sw.plainhex()
        for regexp, excclass in fuzzy_codes:
            if regexp.match(plainhex):
                return excclass
    return CardException


for _e in exceptions:
    locals()[_e.__name__] = _e

def create_sw(sw):
    """Create an exception from an sw tuple."""

    excclass = _sw_match(sw)
    return excclass(sw)

def create_and_raise(sw):
    """Raise an appropriate exception if sw_code is not OK"""
    if sw == SW_OK:
        return
    else:
        # FIXME: not everything not OK is an exception, maybe the warnings can be treated as such?
        raise create_sw(sw)
