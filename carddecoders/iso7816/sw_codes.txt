# copied from http://www.scard.org/software/info.html#SW1%20SW2
# Code	Meaning
90 00	 No further qualification - command successful
61 xx	SW2 indicates the number of response bytes still available. Use GET RESPONSE to access this data.
62 xx	 Warning - state unchanged
62 00	 Warning - no information provided
62 81	 Warning - part of returned data may be corrupt
62 82	 Warning - end of file/record reached (bad cmd)
62 83	 Warning - selected file invalidated
62 84	 Warning - bad file control information format
63 xx	 Warning - state unchanged
63 00	 Warning - no information provided
63 81	 Warning - file filled up with last write
63 Cx	 Warning - counter value is x
64 xx	 Error - state unchanged
65 xx	 Error - state changed
65 00	 Error - no information provided
65 81	 Error - memory failure
66 xx	 Security Error
67 00	 Check Error - wrong length
68 xx	 Check Error - CLA function not supported
68 00	 Check Error - no information provided
68 81	 Check Error - logical channel not supported
68 82	 Check Error - secure messaging not supported
69 xx	 Check Error - command not allowed
69 00	 Check Error - no information provided
69 81	 Check Error - command incompatible with file structure
69 82	 Check Error - security status not satisfied
69 83	 Check Error - authentication method blocked
69 84	 Check Error - referenced data invalidated
69 85	 Check Error - conditions of use not satisfied
69 86	 Check Error - command not allowed (no current EF)
69 87	 Check Error - expected SM data objects missing
69 88	 Check Error - SM data objects incorrect
6A xx	 Check Error - wrong parameters
6A 00	 Check Error - no information provided
6A 80	 Check Error - incorrect parameters in data field
6A 81	 Check Error - function not supported
6A 82	 Check Error - file not found
6A 83	 Check Error - record not found
6A 84	 Check Error - not enough memory space in the file
6A 85	 Check Error - Lc inconsistant with TLV structure
6A 86	 Check Error - inconsistant parameters P1-P2
6A 87	 Check Error - Lc inconsistant with P1-P2
6A 88	 Check Error - referenced data not found
6B 00	 Check Error - wrong parameters
6C xx	 Check Error - wrong length - xx is the correct length
6D 00	 Check Error - instruction code not supported or invalid
6E 00	 Check Error - Class not supported
6F 00	 Check Error - no precise diagnosis
