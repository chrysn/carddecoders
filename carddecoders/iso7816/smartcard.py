"""Provides a subclass of the python-smartcard CardConnection that makes use of
all the extensions provided in this package.

Instead of using a smartcard.CardConnectionDecorator.CardConnectionDecorator,
use the subclasses defined here (CardConnection, LoggingCardConnection).
"""

from __future__ import absolute_import

from ..bytestring import ByteString
from . import sw
from .apdu import DirectAPDUConnection
from .. import conversation

import smartcard.CardConnectionDecorator

class BaseConnection(DirectAPDUConnection):
    """Mixin for CardConnection and replay connections"""

    def check_transmit(self, bytes):
        """Like transmit, but will raise an exception if sw is no ok. Only
        returns data."""
        # this follows the style of subprocess instead of going the pyscard way
        # of using error handlers

        data, sw = self.transmit(bytes)
        if isinstance(sw, Exception):
            raise sw
        return data

# it's nice of the smartcard author to add a decorator you can monkey patch,
# but i prefer to leave the library intact in case i run in the same process as
# someone who wants the original thing.
class CardConnection(smartcard.CardConnectionDecorator.CardConnectionDecorator, BaseConnection):
    """CardConnection enhanced by

    * ByteString results to getATR and transmit
    * check_transmit
    * APDU commands
    """

    def transmit(self, bytes, protocol=None):
        # data, sw1, sw2 = super(CardConnection, self).transmit(list(bytes.intiter()), protocol) # i can haz new style classes kthxbai
        data, sw1, sw2 = smartcard.CardConnectionDecorator.CardConnectionDecorator.transmit(self, list(bytes.intiter()), protocol)
        return ByteString(data), sw.create_sw(ByteString((sw1, sw2)))

    def getATR(self):
        return ByteString(smartcard.CardConnectionDecorator.CardConnectionDecorator.getATR(self))

class LoggingCardConnection(CardConnection):
    """"Connection that logs all its transmissions in a .conversation."""
    # not implemented using pyscard's observer framework as it does not emit
    # reset/atr events

    def __init__(self, *args, **kwargs):
        # again, old style
        #super(LoggingCardConnection, self).__init__(*args, **kwargs)
        CardConnection.__init__(self, *args, **kwargs)

        self.conversation = conversation.Conversation()

    def transmit(self, bytes): # not accepting protocol as it could not be logged with current format
        # old style
        #data, sw = super(LoggingCardConnection, self).transmit(bytes)
        data, sw = CardConnection.transmit(self, bytes)
        self.conversation.append(conversation.Request(bytes, data, sw))
        return data, sw

    def getATR(self):
        # old style
        #atr = super(LoggingCardConnection, self).getATR()
        atr = CardConnection.getATR(self)
        self.conversation.append(conversation.Reset(atr))
        return atr

    def get_conversation(self):
        # FIXME: this will vanish as soon as smartcards are new class objects; then, __getattribute__ can be used by the nonblocking_proxy
        return self.conversation
