"""A very rough implementation of the APDU commands I need for reproducing
sniffed data.

In theory, this follows ISO7816, but it can easily contain errors resulting
from my lack of understanding. (Especially, I am unsure about cla and data
cases)"""

from __future__ import absolute_import

import smartcard.CardConnectionDecorator # not required for APDUs to work, but needed as a base class to DirectAPDUConnection

class APDU(object):
    """Generic APDU.

    When serializing the command by iterating over it, the `cla`, `ins`, `p1`
    and `p2` fields are sent, followed by data depending on the precence of
    `data` and `le` (compare ISO7816-4 5.3)."""
    def intiter(self): # quack like a ByteString
        yield self.cla
        yield self.ins
        yield self.p1
        yield self.p2

        if hasattr(self, 'data'):
            yield len(self.data) # FIXME: for "longer" data, there are special conventions in 4-5.3.2
            for d in self.data:
                yield d

        if hasattr(self, 'le'):
            yield self.le

    def plainhex(self):
        # actually, that's not "quacking like", but plain copying from
        # ByteString's voice. FIXME this calls for a base class
        return " ".join("%02x"%x for x in self.intiter())


class SelectFile(APDU):
    """See 4-6.11. Of the possible values of p1 and p2, only p1=0x04
    ("Selection by DF name" / "Direct selection by DF name") is implemented as
    `direct_df`. Implementation might change when more flags are added."""
    cla = 0
    ins = 0xa4
    p2 = 0

    def __init__(self, address, direct_df=False):
        self.data = address
        self.direct_df = direct_df

    p1 = property(lambda self: 0x04 if self.direct_df else 0x00)

    def __repr__(self):
        return 'SelectFile([%s]%s)'%(", ".join('%#04x'%x for x in self.data), ', direct_df=True' if self.direct_df else '')

class ReadBinary(APDU):
    """See 4-6.1"""
    cla = 0
    ins = 0xb0

    p1 = property(lambda self: 0 if self.short_ef is None else 0x80|self.short_ef)

    def __init__(self, length=0, offset=0, short_ef=None):
        self.le = length
        self.p2 = offset
        self.short_ef = short_ef

    def _set_short_ef(self, short_ef):
        if short_ef is not None and short_ef not in xrange(2<<4):
            raise ValueError("short_ef has to fit in 3 bit")
        else:
            self._short_ef = short_ef
    short_ef = property(lambda self: self._short_ef, _set_short_ef)

    def __repr__(self):
        attrs = []
        if self.le != 0:
            attrs.append('length=%d'%self.le)
        if self.p2 != 0:
            attrs.append('offset=%d'%self.p2)
        if self.short_ef is not None:
            attrs.append('short_ef=%d'%self.short_ef)

        return 'ReadBinary(%s)'%', '.join(attrs)

class ReadRecord(APDU):
    """See 4-6.5"""
    cla = 0
    ins = 0xb2
    p2 = 0x04 # means "in Currently selected EF, Usage of record number in P1, Read record P1"; the only one implemented until now as i don't have a card to test the others, Quick only accepts "read one record"

    def __init__(self, record_number, length=0):
        self.p1 = record_number
        self.le = length

    def __repr__(self):
        return 'ReadRecord(%d)'%self.p1 if self.le == 0 else 'ReadRecord(%d, %d)'%(self.p1, self.le)


def reverse_construct(apdu):
    """Create a more meaningful / descriptive object from a given APDU in
    BinaryString form.

    The result won't be a BinaryString any more, but will quack like one.

    This function will take care never to return something that yields
    different intiter values."""

    if len(apdu) < 4:
        # can't even split
        return apdu

    cla = apdu[0]
    ins = apdu[1]
    p1 = apdu[2]
    p2 = apdu[3]
    lc_data_le = apdu[4:]

    # to my current understanding of the cases described in ISO7816-4-5.3, it could work like this:
    odd_length = False
    if not lc_data_le: # why not
        lc = None
        data = None
        le = None
    elif len(lc_data_le) == 1: # has to be before [0] == len-1 because it means that in ReadBinary with le=0.
        le = int(lc_data_le[0])
        lc = None
        data = None
    elif int(lc_data_le[0]) == len(lc_data_le) - 2:
        lc = int(lc_data_le[0])
        le = int(lc_data_le[-1])
        data = lc_data_le[1:-1]
    elif int(lc_data_le[0]) == len(lc_data_le) - 1:
        le = None
        lc = int(lc_data_le[0])
        data = lc_data_le[1:]
    else:
        # odd length, give up
        return apdu

    enhanced = None

    if cla == 0x00 and ins == 0xa4:
        if (p1, p2) == (0x00, 0x00):
            enhanced = SelectFile(data)
        elif (p1, p2) == (0x04, 0x00):
            enhanced = SelectFile(data, direct_df=True)

    elif ins == 0xb0:
        if int(p1) & 0x80:
            short_ef = int(p1) & ~0x80
            if short_ef not in xrange(2<<4):
                return apdu # can't be represented as ReadBinary as specified in 4-6.1
        else:
            short_ef = None
            if not p1 == 0:
                return apdu # according to 4-6.1, this means that offset=p1|p2, which is unsupported by the ReadBinary class
        enhanced = ReadBinary(length=int(le), offset=int(p2), short_ef=short_ef)

    elif ins == 0xb2:
        if p2 == 0x04:
            enhanced = ReadRecord(p1, le)

    if enhanced is None:
        # didn't find anything
        return apdu


    if list(enhanced.intiter()) != list(apdu.intiter()):
        print "what happen? am i doing it wrong?" # FIXME
        return apdu
    else:
        return enhanced


class DirectAPDUConnection(smartcard.CardConnectionDecorator.CardConnectionDecorator): # deriving from there is not really necessary as far as model logic is concerned (this could just as well be seen as a mix-in), but this is a sure way for this object to stay old-style as long as pyscard uses old style classes
    """Mixin for `smartcard.Connection` and similar objects. Converts calls
    like

    c.selectFile(...)

    to

    c.check_transmit(SelectFile(...))

    for more readable code and easier profiling (calls show up as different in
    the call tree)"""

    # FIXME: i want those automated (especially the arguments and their
    # defaults), but still retain meaningful signatures

    def selectFile(self, address, direct_df=False):
        return self.check_transmit(SelectFile(address, direct_df))

    def readBinary(self, length=0, offset=0, short_ef=None):
        return self.check_transmit(ReadBinary(length, offset, short_ef))

    def readRecord(self, record_number, length=0):
        return self.check_transmit(ReadRecord(record_number, length))
