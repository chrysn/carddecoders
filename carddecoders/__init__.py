"""Helpers for and extensions to python-smartcard"""

class AutoconnectFailure(Exception):
    """Autoconnect was used in a situation too complicated for autoconnect."""

class PromisingDataException(Exception):
    """Exception to be raised when parsed data does not look like it always
    looked like before, providing the possibility to gather more
    information."""

class SpecViolation(Exception):
    """There is a specification that forbids data to look like this, yet it
    does."""

def test():
    import doctest

    from . import bytestring
    from .iso7816 import smartcard, sw
    from . import en1546
    from . import money
    from . import signatures

    doctest.testmod(bytestring)
    doctest.testmod(sw)
    doctest.testmod(smartcard)
    doctest.testmod(en1546)
    doctest.testmod(money)
    doctest.testmod(signatures)
