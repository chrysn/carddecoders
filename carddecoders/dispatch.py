"""Module for easy dispatching of card connections.

The created connections are already fully patched by the iso7816.smartcard
module (i.e., speak ByteString, have check_transmit and take APDU commands).
"""

from __future__ import absolute_import

import sys

import smartcard.System
from smartcard.pcsc.PCSCCardConnection import PCSCCardConnection
from smartcard.Exceptions import CardConnectionException

from . import AutoconnectFailure
from .iso7816.smartcard import CardConnection, LoggingCardConnection

class ConnectionDispatcher(object):
    """Object that spawns smart card connections.

    Using this object eases creating connections living in a
    multiprocessing.managers managed process."""

    # FIXME: check if managing readers as strings is really necessary now that
    # the reader objects can be packed in multiprocessing.managers proxies
    # (this was necessary when the managers feature was created manually in the
    # async module)
    #
    # testing this requires list_Readers and dispatch to be exposed by the
    # proxy in the first place

    def list_readers(self):
        return [r.name for r in smartcard.System.readers()]

    def _reader_name2object(self, reader):
        for r in smartcard.System.readers():
            if r.name == reader:
                return r
        raise Exception("Unknown reader")

    def dispatch(self, reader, logging=False):
        # originally, this used:
        #   r = self._reader_name2object(reader)
        #   c = r.createConnection()
        #   enhance(c, logging=logging)
        # where enhance exchanged the classes.
        #
        # given the simplicity of createConnection (at the time of writing, it
        # was a plain
        #   return CardConnectionDecorator( PCSCCardConnection( self.name ) )
        # which does not look as if it would change in forseeable future), i
        # think this approach is more elegant as it does not involve any
        # post-initialization class juggling:

        baseconnection = PCSCCardConnection(reader)
        if logging:
            c = LoggingCardConnection(baseconnection)
        else:
            c = CardConnection(baseconnection)

        c.connect()

        return c

    def autodispatch(self, logging=False, signature=None):
        if signature is None:
            candidates = self.list_readers()
        else:
            candidates = []
            for r in self.list_readers():
                try:
                    atrcon = self.dispatch(r)
                except CardConnectionException:
                    continue # FIXME: issue warning that reader could not be selected
                try:
                    atr = atrcon.getATR()
                except CardConnectionException:
                    continue # FIXME: issue warning that reader could not be selected
                finally:
                    atrcon.disconnect()
                if signature.match_atr(atr):
                    candidates.append(r)
        if len(candidates) != 1:
            raise AutoconnectFailure()
        (reader, ) = candidates
        return self.dispatch(reader, logging=logging)
