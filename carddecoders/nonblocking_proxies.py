"""nonblocking proxies for classes in carddecoders"""

from nonblocking import AsyncBaseProxy
from nonblocking.events import GEventSupportingAsyncManager, GEventPassingProxy, Event
from .dispatch import ConnectionDispatcher
from .en1546.quick import QuickCard

import gobject

class ConnectionDispatcherProxy(AsyncBaseProxy):
    _method_to_typeid_ = {'autodispatch': 'ResultingConnection'}

    def blocking_autodispatch(self, logging=False, signature=None):
        return self._callmethod('autodispatch', (logging, signature))

class ConnectionProxy(AsyncBaseProxy):
    """Connections are, as of now, never directly used via the managers
    interface, but only passed to other proxy objects where they are
    un-proxied.

    If you want to use this directly in an async way, keep in mind that every
    command you send to the card might fail, but queued commands will be sent
    afterwards nevertheless, and the smartcard connection will usually have no
    way of telling which state the ard is currently in, so you might want to
    keep the card control logic close to the card, in the managed process."""

    _exposed_ = ('get_conversation', )

    conversation = property(lambda self: self._callmethod('get_conversation', ())) # can be pickled/unpickled easily, only blocks for the transmission of the conversation

class EventSendingQuickCard(QuickCard):
    """When using events instead of callbacks, wrapped object has to change."""
    def add_connection(self, connection):
        def send_progress(progress):
            self._event_processor(Event("progress", (progress,)))
        return super(EventSendingQuickCard, self).add_connection(connection, progress_callback=send_progress)

    def set_event_processor(self, processor):
        self._event_processor = processor

    def _update_or_insert_record(self, transaction):
        super(EventSendingQuickCard, self)._update_or_insert_record(transaction)

        self._event_processor(Event('transaction-updated', (transaction.nt_iep, self.transactions[transaction.nt_iep])))


class QuickCardProxy(GEventPassingProxy, gobject.GObject):
    _exposed_ = ('add_connection', '__getattribute__', )

    __gproperties__ = {
            'current_money': (gobject.TYPE_PYOBJECT, 'current money', '', gobject.PARAM_READWRITE),
            'limit': (gobject.TYPE_PYOBJECT, 'limit', '', gobject.PARAM_READWRITE),
            'expiry_date': (gobject.TYPE_PYOBJECT, 'expiry date', '', gobject.PARAM_READWRITE),
            'card_id': (gobject.TYPE_PYOBJECT, 'card id', 'unique number of this quick card', gobject.PARAM_READWRITE),
            'routingcode': (gobject.TYPE_PYOBJECT, 'routing code', '', gobject.PARAM_READWRITE),
            'card_number': (gobject.TYPE_PYOBJECT, 'card number', 'increments for every card issued for the given bank account', gobject.PARAM_READWRITE),
            'account_number': (gobject.TYPE_PYOBJECT, 'account number', '', gobject.PARAM_READWRITE),
            }

    __gsignals__ = {
            'progress': (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_FLOAT, )),
            'transaction-updated': (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_INT, gobject.TYPE_PYOBJECT, )),
            }

    def __init__(self, *args, **kwargs):
        GEventPassingProxy.__init__(self, *args, **kwargs)
        gobject.GObject.__init__(self)

        self.__properties = {}

    def do_get_property(self, prop):
        return self.__properties[prop.name]

    def do_set_property(self, prop, value):
        self.__properties[prop.name] = value

    def add_connection(self, connection, on_success, on_error):
        self._async_callmethod('add_connection', (connection, ), {}, self.__on_connection_read(on_success), on_error)

    def __on_connection_read(self, callback):
        def connection_read(none):
            for prop in ('current_money', 'limit', 'expiry_date', 'card_id', 'routingcode', 'card_number', 'account_number'):
                self._async_callmethod('__getattribute__', (prop,), {}, lambda value, prop=prop: self.__update_prop(prop, value), self.__on_unexpected_error)

                callback()
        return connection_read

    def __update_prop(self, prop, value):
        self.set_property(prop, value)

    def __on_unexpected_error(self, error):
        print "unexpected error", error
        # FIXME

class GCardFrameworkManager(GEventSupportingAsyncManager):
    """Manager that has all known card framework objects registered"""

GCardFrameworkManager.register('ResultingConnection', proxytype=ConnectionProxy)
GCardFrameworkManager.register('ConnectionDispatcher', ConnectionDispatcher, proxytype=ConnectionDispatcherProxy)
GCardFrameworkManager.register('QuickCard', EventSendingQuickCard, proxytype=QuickCardProxy)
