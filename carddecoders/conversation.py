"""Model describing a card conversation.

A Conversation ist a list of several stanzas; a stanza is either a Request or a
Reset; both have an .answer, a Request additionally has a .request and a .sw.

All objects can be .serialize()d; that will produce a string in the style of
the pcsc-tools' scriptor tool."""


from .bytestring import ByteString as B
from .iso7816.sw import create_sw
from .iso7816 import apdu
try:
    from collections import namedtuple
except ImportError:
    from .future import namedtuple


class Conversation(list):
    def serialize(self):
        """Dump the conversation to a human readable format with hardly any
        interpretation (only error codes are explained). The format tries to
        emulate the output of pcsc-tools' scriptor tool."""
        return "".join(x.serialize() for x in self)

    def python(self):
        """Generate a python replay of the conversation."""
        return '\n'.join(x.python() for x in self)

    @classmethod
    def deserialize(cls, dump):
        """Reproduce a Conversation from the output of serialize or similar data."""

        # error handling
        stanza_number = None
        def parse_assert(condition, message):
            """Raise ParseError(message) unless condition is True"""
            if stanza_number is not None:
                message = "%s (stanza %s)"%(message, stanza_number)
            if not condition:
                raise cls.ParseError(message)

        result = cls()

        lines = filter(lambda l: l, dump.split('\n'))

        parse_assert(len(lines)%2 == 0, "Odd number of lines.")
        stanzas = zip(lines[:-1:2], lines[1::2])

        for stanza_number, (line_out, line_in) in enumerate(stanzas):
            parse_assert(line_out[:2] == '> ' and line_in[:2] == '< ', "Unknown direction.")

            line_out = line_out[2:]
            line_in = line_in[2:]

            if line_out.strip() == 'RESET':
                parse_assert(line_in[:4] == 'OK: ', 'Non-OK reset is unknown.')
                try:
                    atr = B(line_in[4:].strip())
                except ValueError:
                    raise cls.ParseError('Malformed ATR')
                result.append(Reset(atr))
            else:
                try:
                    request = B(line_out.strip())
                except ValueError:
                    raise cls.ParseError('Malformed request')
                try:
                    response_sw = B(line_in.split(':', 1)[0].strip()) # everything behind the colon is ignored
                except ValueError:
                    raise cls.ParseError('Malformed response')

                response = response_sw[:-2]
                sw = response_sw[-2:]

                # try to get higher level objects
                request = apdu.reverse_construct(request)
                sw = create_sw(sw)

                result.append(Request(request, response, sw))

        return result

    class ParseError(Exception):
        """Raised whenever data to be serialized is not recognized."""


class Request(namedtuple('RequestTuple', 'request, response, sw')):
    __slots__ = ()

    def serialize(self):
        """Dump the request to Conversation.serialize's format."""
        return '> %s\n< %s %s : %s\n'%(self.request.plainhex(), self.response.plainhex(), self.sw.plainhex(), self.sw.message)

    def python(self):
        """Generate a python replay of this request."""
        if self.response:
            return 'card.transmit(%r)\n# %s, %s\n'%(self.request, self.response.plainhex(), self.sw.message)
        else:
            return 'card.transmit(%r)\n# %s\n'%(self.request, self.sw.message)


class Reset(namedtuple('ResetTuple', 'answer')):
    __slot__ = ()

    def serialize(self):
        """Dump the reset to Conversation.serialize's format."""
        return '> RESET\n< OK: %s\n'%self.answer.plainhex()

    def python(self):
        """Generate a python replay of the reset."""
        return 'card.getATR()\n# %s\n'%self.answer.plainhex()
