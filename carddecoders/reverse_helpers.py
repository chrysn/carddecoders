"""Simple functions to ease reverse engineering ByteString objects

Designed for interacive use, some functions take an additional argument so they
give return values instead of printing."""

def backward_length(s):
    """List bytes whose value indicates the length of the remaining string"""
    ls = len(s)

    for (i, v) in enumerate(s.intiter()):
        if v == ls - (i + 1):
            print "index %d: %d remaining"%(i, v)

def contains_number(s, number, return_list=False):
    """Try encoding number in different encodings and check if it is contained in s"""

    number_str = str(number)

    found = []

    # bcd encoding
    s_hex = s.plainhex().replace(' ','')
    if number_str in s_hex:
        found.append("number found in BCD at offset %s bytes"%(s_hex.index(number_str)/2.0))

    # decimal encoding
    s_ascii = s.decode('ascii', 'replace')
    if number_str in s_ascii:
        found.append("number found in ascii encoding at offset %s bytes"%s_ascii.index(number_str))

    # binary big endian
    number_hex = "%x"%number
    if number_hex in s_hex:
        found.append("number found in big endian encoding ending at %s bytes"%((s_hex.index(number_hex) + len(number_hex))/2.0))

    if return_list:
        return found
    else:
        print "\n".join(found)
