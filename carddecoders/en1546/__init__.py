# encoding: utf-8
"""Tools for smart cards according to EN1546 "Identification card systems --
Inter-sector electronic purse" (IEP)"""

from .. import SpecViolation, PromisingDataException
from .. import money
from ..bytestring import MeaningfulByteString, caching_property

def decode_money(value, currency, strict=True):
    """Convert a 4-byte value ByteString and a 3-byte currency to a Money
    object.

    This is not a MeaningfulByteString because value and currency are typically
    spread out over a longer or even differen ByteStrings, making it
    impracticable to slice.

    The interpretation follows the EN for the MTOT and CURR_IEP fields: value
    is a 4-byte unsigned big endian integer in the currency's lowest units,
    currency's 0C CC DD, where 0 is always 0, CCC is BCD for the currency's
    ISO4217 code and DD is the position of the decimal point. (Not sure about
    the last byte, as it was always 00 up to now).

    For compatibility with Austrian Quick IEP cards, also accepts 2-byte
    currency values and a parameter to avoid `strict` checking for first four
    bits of currency to be 0.

    >>> from ..bytestring import ByteString as B
    >>> decode_money(B("00 00 01 00"), B("09 78 00"))
    <EUR 256e-2>
    >>> print unicode(_).encode('utf-8')
    € 2.56
    """

    if len(value) != 4 or len(currency) not in (2, 3):
        raise ValueError("value must be 4, currency 2-3 byte long")

    if strict:
        must_be_null = currency[0].decode_bcd(use_last_nibble=False)
        if must_be_null != 0:
            raise SpecViolation("EN1546 3-6.2.8 dictates that the highest 4 byte must be set to 0.")

    if len(currency) > 2 and currency[2] != '\x00':
        raise PromisingDataException("Position of the decimal point is not zero (%r)."%currency[2])

    return money.Money(value.int_bigendian(), money.Currency(currency[0:2].decode_bcd(use_first_nibble=False)))
