import gtk

class ProgressStatusbar(gtk.Statusbar):
    __gtype_name__ = "ProgressStatusbar"
    def __init__(self):
        super(ProgressStatusbar, self).__init__()

        self.progressbar = gtk.ProgressBar()
        self.pack_end(self.progressbar)
