# encoding: utf-8

import gtk
from carddecoders.gtktools import safecb

_ = lambda x:x

class QuickIndicatorArea(gtk.Expander):
    __gtype_name__ = "QuickIndicatorArea"
    def __init__(self):
        self._card = None

        super(QuickIndicatorArea, self).__init__()

        self.props.expanded = True
        self.connect('notify::expanded', safecb(self.cb_expand_changed))

        hbox = gtk.HBox()
        self.add(hbox)

        self.amountlabel = gtk.Label()
        self.amountlabel.props.use_markup = True
        hbox.pack_start(self.amountlabel, True)

        grid = gtk.Table(rows=6, columns=1)
        grid.attach(gtk.Label(_('Limit:')), 0, 1, 0, 1)
        grid.attach(gtk.Label(_('Expires:')), 0, 1, 1, 2)
        grid.attach(gtk.Label(_('Card ID:')), 0, 1, 2, 3)
        grid.attach(gtk.Label(_('Routing Code:')), 0, 1, 3, 4)
        grid.attach(gtk.Label(_('Account number:')), 0, 1, 4, 5)
        grid.attach(gtk.Label(_('Card number:')), 0, 1, 5, 6)

        self.limitlabel = gtk.Label()
        self.expireslabel = gtk.Label()
        self.idlabel = gtk.Label()
        self.routingcodelabel = gtk.Label()
        self.cardnumberlabel = gtk.Label()
        self.accountnumberlabel = gtk.Label()
        grid.attach(self.limitlabel, 1, 2, 0, 1, xpadding=12)
        grid.attach(self.expireslabel, 1, 2, 1, 2, xpadding=12)
        grid.attach(self.idlabel, 1, 2, 2, 3, xpadding=12)
        grid.attach(self.routingcodelabel, 1, 2, 3, 4, xpadding=12)
        grid.attach(self.accountnumberlabel, 1, 2, 4, 5, xpadding=12)
        grid.attach(self.cardnumberlabel, 1, 2, 5, 6, xpadding=12)

        for c in grid.get_children():
            c.set_alignment(0, 0.5) # align left

        hbox.pack_start(grid, False)

    def _set_card(self, card):
        self._card = card
        if card is None:
            self.props.expanded = False
            self.props.sensitive = False
        else:
            self.props.expanded = True
            self.props.sensitive = True

            self.card.connect('notify::current-money', safecb(self.cb_currentmoney_changed))
            self.card.connect('notify::expiry-date', safecb(self.cb_expirydate_changed))
            self.card.connect('notify::limit', safecb(self.cb_limit_changed))
            self.card.connect('notify::card-id', safecb(self.cb_cardid_changed))
            self.card.connect('notify::routingcode', safecb(self.cb_routingcode_changed))
            self.card.connect('notify::card-number', safecb(self.cb_cardnumber_changed))
            self.card.connect('notify::account-number', safecb(self.cb_accountnumber_changed))

    def cb_expirydate_changed(self, card, prop):
        self.expireslabel.props.label = unicode(card.props.expiry_date)
    def cb_currentmoney_changed(self, card, prop):
        self.amountlabel.props.label = u'<big><big><big><big>%s</big></big></big></big>'%(card.props.current_money, )
        self.cb_expand_changed()
    def cb_limit_changed(self, card, prop):
        self.limitlabel.props.label = unicode(card.props.limit)
    def cb_cardid_changed(self, card, prop):
        self.idlabel.props.label = unicode(card.props.card_id)
    def cb_routingcode_changed(self, card, prop):
        self.routingcodelabel.props.label = unicode(card.props.routingcode)
    def cb_cardnumber_changed(self, card, prop):
        self.cardnumberlabel.props.label = unicode(card.props.card_number)
    def cb_accountnumber_changed(self, card, prop):
        self.accountnumberlabel.props.label = unicode(card.props.account_number)

    card = property(lambda self: self._card, _set_card)

    def cb_expand_changed(self, *args):
        if self.card:
            self.props.label = _('Details') if self.props.expanded else _(u'Details – Current amount: %s')%(self.card.props.current_money, )
        else:
            self.props.label = _('No card loaded')
