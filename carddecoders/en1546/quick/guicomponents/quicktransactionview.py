# encoding: utf-8

import gtk

from carddecoders.gtktools import safecb
from ... import quick
from ...quick import addresses

_ = lambda x: x

class QuickTransactionView(gtk.TreeView):
    __gtype_name__ = "QuickTransactionView"
    def __init__(self):
        gtk.TreeView.__init__(self)

        def cdf_text(fn):
            def cdf(column, cell, model, iter):
                record = model.get_value(iter, 0)
                cell.props.text = fn(record)
            return safecb(cdf)

        def cdf_pixmap(fn):
            def cdf(column, cell, model, iter):
                record = model.get_value(iter, 0)
                cell.props.stock_id = fn(record)
            return safecb(cdf)

        self.props.headers_clickable = True

        cell_number = gtk.CellRendererText()
        col_number = gtk.TreeViewColumn(_('#'), cell_number)
        col_number.set_cell_data_func(cell_number, cdf_text(lambda record: str(record.nt_iep)))
        col_number.props.sort_column_id = 1
        self.append_column(col_number)

        col_amount = gtk.TreeViewColumn(_('Amount'))
        cell_direction_icon = gtk.CellRendererPixbuf()
        #cell_direction_text = gtk.CellRendererText()
        cell_amount = gtk.CellRendererText()
        col_amount.pack_start(cell_direction_icon, False)
        #col_amount.pack_start(cell_direction_text, True)
        col_amount.pack_start(cell_amount, True)
        def record2pixmap(record):
            if isinstance(record, quick.LoadTransaction):
                return gtk.STOCK_ADD
            elif isinstance(record, quick.PurchaseTransaction):
                if record.delta.value:
                    return gtk.STOCK_REMOVE
                else:
                    return gtk.STOCK_INFO
            elif isinstance(record, quick.PinCashTransaction):
                return 'cash'
            elif isinstance(record, quick.PinPurchaseTransaction):
                return gtk.STOCK_REMOVE
            return gtk.STOCK_HELP
        col_amount.set_cell_data_func(cell_direction_icon, cdf_pixmap(record2pixmap))
        #col_amount.set_cell_data_func(cell_direction_text, cdf_text(lambda record: _('In') if isinstance(record, quick.LoadTransaction) else _('Out')))
        col_amount.set_cell_data_func(cell_amount, cdf_text(lambda record: unicode(record.delta)))
        col_amount.props.sort_column_id = 2
        self.append_column(col_amount)

        cell_left = gtk.CellRendererText()
        col_left = gtk.TreeViewColumn(_('Remaining'), cell_left)
        col_left.set_cell_data_func(cell_left, cdf_text(lambda record: unicode(record.remaining) if hasattr(record, 'remaining') else u'–'))
        col_left.props.sort_column_id = 4
        self.append_column(col_left)

        col_date = gtk.TreeViewColumn(_('Date'))
        cell_date = gtk.CellRendererText()
        cell_time = gtk.CellRendererText()
        col_date.pack_start(cell_date, False)
        col_date.pack_start(cell_time, False)
        col_date.set_cell_data_func(cell_date, cdf_text(lambda record: record.timestamp.strftime("%Y-%m-%d") if record.timestamp else u'–'))
        col_date.set_cell_data_func(cell_time, cdf_text(lambda record: record.timestamp.strftime("%H:%M:%S") if record.timestamp else u'–'))
        col_date.props.sort_column_id = 3
        self.append_column(col_date)

        cell_terminal = gtk.CellRendererText()
        col_terminal = gtk.TreeViewColumn(_('Terminal'), cell_terminal)
        def record2terminal(record):
            if isinstance(record, quick.LoadTransaction):
                try:
                    loc_key = int(record.id_psam)
                except:
                    ValueError
                else:
                    if loc_key in addresses.ATM_LOCATIONS:
                        return ", ".join(addresses.ATM_LOCATIONS[loc_key])
                return record.id_psam
            elif isinstance(record, quick.PurchaseTransaction):
                if record.id_psam in addresses.PSAM_LOCATIONS:
                    return ", ".join(addresses.PSAM_LOCATIONS[record.id_psam])
                else:
                    return str(record.id_psam)
            elif isinstance(record, quick.PinCashTransaction) or isinstance(record, quick.PinPurchaseTransaction):
                return record.text
            elif isinstance(record, quick.UnknownTransaction):
                if hasattr(record, 'description'):
                    return record.description
                if hasattr(record, 'field1'):
                    return "%s %s"%(record.field1, record.field2)
                else:
                    return None
            else:
                return str(record)
        col_terminal.set_cell_data_func(cell_terminal, cdf_text(lambda record: record2terminal(record)))
        self.append_column(col_terminal)

        cell_termnr = gtk.CellRendererText()
        col_termnr = gtk.TreeViewColumn(_('Terminal #'), cell_termnr)
        col_termnr.set_cell_data_func(cell_termnr, cdf_text(lambda record: str(record.nt_psam) if hasattr(record, 'nt_psam') else u'–'))
        self.append_column(col_termnr)

        cell_strange = gtk.CellRendererText()
        col_strange = gtk.TreeViewColumn(_('Strange'), cell_strange)
        def strangeness(record):
            items = []
            if hasattr(record, 'tw'):
                items.append('TW=%#02x'%record.tw)
            if hasattr(record, 'x') and record.x is not None:
                items.append('X=%d'%record.x)
            return ", ".join(items)
        col_strange.set_cell_data_func(cell_strange, cdf_text(strangeness))
        self.append_column(col_strange)
