import pkgutil
from StringIO import StringIO

import csv

try:
    # quick and dirty hack to get cool visual output from atm addresses.
    #
    # the atm addresses are not shipped with this project as there is no free
    # database containing them; if you want to create your own database, write
    # it to a csv file whose first column contains the nt_psam, followed by address fields.
    ATM_LOCATIONS = dict((int(x[0]), x[1:]) for x in csv.reader(StringIO(pkgutil.get_data('carddecoders.en1546.quick', 'atm_addresses.csv'))))
except IOError:
    ATM_LOCATIONS = {}

PSAM_LOCATIONS = dict((int(x[0]), x[1:]) for x in csv.reader(StringIO(pkgutil.get_data('carddecoders.en1546.quick', 'idpsam_addresses.csv'))))
