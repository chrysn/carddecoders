"""Special fields for the Austrian "Quick" IEP card"""

import datetime

from ...bytestring import MeaningfulByteString, caching_property
from ...bytestring import ByteString as B
from ... import SpecViolation, PromisingDataException
from .. import decode_money
from ...money import Money, Currency

class DateYYYDDD(MeaningfulByteString):
    """Date format as used in Quick.

    This format differs from what is usually used in EN1546 in that it uses a
    YYDDD format instead of YYMMDD as specified in section 3-6.2.9. Here, YYY
    are the three lowest BCD digits of the year and DDD is the BCD number of
    the day in the year.

    >>> DateYYYDDD("00 90 01").date
    datetime.date(2009, 1, 1)
    >>> DateYYYDDD("01 13 65").date
    datetime.date(2011, 12, 31)
    """
    LENGTH = 3

    year = caching_property(lambda self: self[0:2].decode_bcd(use_last_nibble=False) + 2000)
    day_of_year = caching_property(lambda self: self[1:3].decode_bcd(use_first_nibble=False))

    date = caching_property(lambda self: datetime.date(self.year, 1, 1) + datetime.timedelta(self.day_of_year - 1))

class DateYYYDDDTime(MeaningfulByteString):
    """Date/Time format as used in Quick.

    First three bytes work as in DateYYYDDD, last three bytes are BCD for hour,
    minute and second, respectively. Seconds are sometimes left blank.

    There is no information on the time zone in the data.

    >>> DateYYYDDDTime("00 90 01 11 59").datetime
    datetime.datetime(2009, 1, 1, 11, 59, 0)
    """
    LENGTH = 5

    date = caching_property(lambda self: DateYYYDDD(self[0:3]).date)
    hour = caching_property(lambda self: self[3].decode_bcd())
    minute = caching_property(lambda self: self[4].decode_bcd())
    # there used to be seconds in this field, but they were always equal 0 or
    # 1, and later obviously part of of another field, so it is assumed that
    # seconds are not used by quick cards

    datetime = caching_property(lambda self: datetime.datetime(self.date.year, self.date.month, self.date.day, self.hour, self.minute, tzinfo=None))

class DateYYMMDD(MeaningfulByteString):
    """Another date format used on Austrian bank cards (in those areas not directly related to quick)"""
    LENGTH = 3
    date = property(lambda self: datetime.date(year=self[0].decode_bcd()+2000, month=self[1].decode_bcd(), day=self[2].decode_bcd()))

class TimeHHMMSS(MeaningfulByteString):
    """Time format typically used with DateYYMMDD"""
    LENGTH = 3
    time = property(lambda self: datetime.time(hour=self[0].decode_bcd(), minute=self[1].decode_bcd(), second=self[2].decode_bcd()))

class CardATR(MeaningfulByteString):
    LENGTH = 24

    card_id = caching_property(lambda self: self[15:19].decode_bcd())

    @caching_property
    def unparsed(self):
        # atr description from debian's /usr/share/pcsc/smartcard_list.txt
        if not self[0:6] == [59, 191, 17, 0, 129, 49] or not self[7:15] == [69, 69, 80, 65, 0, 0, 0, 0] or not self[19:21] == [0, 0]:
            raise SpecViolation("Seems not to be a Quick card.")
        return (self[6], self[-3:])

class LoadRecord(MeaningfulByteString):
    LENGTH = 25

    nt_iep = caching_property(lambda self: self[1:3].int_bigendian())
    delta = caching_property(lambda self: decode_money(self[3:7], self[7:9]))
    id_psam = caching_property(lambda self: self[9:12].encode('hex').lstrip('0'))
    timestamp = caching_property(lambda self: DateYYYDDDTime(self[15:20]).datetime)
    remaining = caching_property(lambda self: decode_money(self[21:25], self[7:9]))

    @caching_property
    def unparsed(self):
        if not any(self.intiter()):
            raise Exception("Can't handle empty records.")
        if not self[0] == 0x07:
            raise PromisingDataException("First byte of LoadRecord is not 0x07 (%r)"%self[0])
        if not self[13:15] == (0x90, 0x00):
            raise PromisingDataException("Bytes 13-14 of LoadRecord are not 0x90 0x00 (%r)"%self[0])
        return (self[20], )

    def __repr__(self):
        return "<LoadRecord#%s %s %r>"%(self.nt_iep, self.timestamp, self.delta)

class PurchaseRecord(MeaningfulByteString):
    LENGTH = 33

    nt_iep = caching_property(lambda self: self[1:3].int_bigendian())
    delta = caching_property(lambda self: decode_money(self[3:7], self[11:13]))
    id_psam = caching_property(lambda self: self[13:17].int_bigendian())
    nt_psam = caching_property(lambda self: self[17:21].int_bigendian())
    timestamp = caching_property(lambda self: DateYYYDDDTime(self[23:28]).datetime)
    remaining = caching_property(lambda self: decode_money(self[29:33], self[11:13]))

    @caching_property
    def unparsed(self):
        if not any(self.intiter()):
            raise Exception("Can't handle empty records.")
        if not self[21:23] == (0x90, 0x00):
            raise PromisingDataException("Bytes 21-22 of PurchaseRecord are not 0x90 0x00 (%r)"%self[0])
        if not (self[0] == 0x23 or self[0] == 0x27):
            raise PromisingDataException("New value for first byte of PurchaseRecord")
        # 7:11 is the same as 3:7 most of the time, but sometimes it's not.
        return (self[0], self[7:11] if self[7:11] != self[3:7] else None, self[28])

    def __repr__(self):
        return "<PurchaseRecord#%s %s %r>"%(self.nt_iep, self.timestamp, self.delta)

class File_3f00_0002(MeaningfulByteString): # previously called data1
    LENGTH = 36

    routingcode = caching_property(lambda self: self[5:8].decode_bcd(use_first_nibble=False))
    expiry_date = caching_property(lambda self: DateYYYDDD(self[17:20]))

    @caching_property
    def unparsed(self):
        # FIXME: the rest is not yet decoded
        pass

class File_df01_0101(MeaningfulByteString): # previously called data2
    LENGTH = 38

    limit = caching_property(lambda self: decode_money(self[23:27], self[21:23]))
    expiry_date = caching_property(lambda self: DateYYYDDD(self[11:14]))

    @caching_property
    def unparsed(self):
        # FIXME: [17:20] contains a date 1y before expiry date; the rest is not yet decoded
        pass

class File_df01_0102(MeaningfulByteString): # previously called data4
    LENGTH = 4

class Record_df01_applet34_0003(MeaningfulByteString):
    LENGTH = 28

    delta = caching_property(lambda self: decode_money(self[19:23], self[23:25], strict=False) if any(self[19:25].intiter()) else None)
    text = caching_property(lambda self: self[7:19].decode('ascii').strip())
    timestamp = caching_property(lambda self: DateYYYDDDTime(self[2:7]).datetime)
    nt_iep = caching_property(lambda self: self[0:2].int_bigendian())

    @caching_property
    def unparsed(self):
        return (int(self[23])>>4, None if self[25:28] == (0x30, 0x90, 0x00) else self[25:28]) # >>4 from strict=False in decode_money

def dateplustime(date, time):
    return datetime.datetime(date.year, date.month, date.day, time.hour, time.minute, time.second)

class Record_df01_applet19_001d(MeaningfulByteString):
    LENGTH = 58

    nt_iep = caching_property(lambda self: self[0:2].int_bigendian())
    timestamp = caching_property(lambda self: dateplustime(DateYYMMDD(self[17:20]).date, TimeHHMMSS(self[21:24]).time) if any(self[17:20].intiter()) or any(self[21:24].intiter()) else None)
    delta = caching_property(lambda self: Money(self[2:8].decode_bcd(), Currency(self[15:17].decode_bcd())))
    description = caching_property(lambda self: self[28:51].decode('ascii').strip('\0').strip(' '))

    @caching_property
    def unparsed(self):
        return (self[8:15], self[20], self[24:28], self[51:], )

class Record_df09_001d(MeaningfulByteString):
    LENGTH = 63

    delta = caching_property(lambda self: Money(self[0:6].decode_bcd(), Currency(self[6:8].decode_bcd())) if any(self[0:8].intiter()) else None)
    nt_iep = caching_property(lambda self: self[14:18].int_bigendian())

    timestamp = caching_property(lambda self: dateplustime(DateYYMMDD(self[8:11]).date, TimeHHMMSS(self[21:24]).time) if self.delta is not None else None)

    field1 = caching_property(lambda self: (self[30:41].decode('ascii').strip('\0'), self[41:45].decode('ascii').strip('\0')) if self.delta is not None else None)
    field2 = caching_property(lambda self: self[45:56].decode('ascii').strip('\0') if self.delta is not None else None) # FIXME: interpretation needs more checking with other cards

    @caching_property
    def unparsed(self):
        if self.delta is None:
            return (self[8:14], self[18:])
        else:
            if any(self[26:30].intiter()):
                raise PromisingDataException("Record does not look like expected (%r)."%self[26:30])
            return (self[11:14], self[18:21], self[24:26], self[56:])

class Response_dfiep(MeaningfulByteString):
    # see en1546-3:1999 table 4
    def __init__(self, otherself):
        if not self[0] == 0x6f:
            raise SpecViolation("Response does not start with 6F")
        if int(self[1]) != len(self)-2:
            raise SpecViolation("Wrong length")

        self.unparsed = {}

        remaining = self[2:]
        while remaining:
            field_length = int(remaining[1])
            field_contents = remaining[2:2+field_length]
            if len(field_contents) != field_length:
                raise SpecViolation("Wrong length")
            if remaining[0] == 0x84:
                self.df_name = field_contents
            else:
                self.unparsed[int(remaining[0])] = field_contents
            remaining = remaining[2+field_length:]

class FieldBasedData_df09_0001(MeaningfulByteString):
    LABELS = {
            B('8e'): '8e',
            B('5f 25'): 'creation date (?)',
            B('5f 24'): 'expiry date',
            B('9f 07'): '9f 07',
            B('5a'): 'account number',
            B('5f 34'): 'card number',
            B('5f 28'): '5f 28',
            B('9f 0d'): '9f 0d',
            B('9f 0e'): '9f 0e',
            B('9f 0f'): '9f 0f',
            B('9f 4a'): '9f 4a', # found in r02
            B('8c 27 9f 02'): '8c 27 9f 02', # unsure, 02 could also be length
            B('2a'): '2a', # unsure, as found behind 8c 27 9f 02
            B('9c 01 9f 21'): '9c 01 9f 21', # still unsure
            B('9f 33'): '9f 33', # it starts getting very likely here again
            B('9f 1c'): '9f 1c',
            B('04 8d'): '04 8d',
            B('5f 28'): '5f 28',
            B('5f 30'): '5f 30',
            B('9f 08'): '9f 08',
            B('9f 42'): '9f 42',
            B('9f 44'): '9f 44',
            B('8c 24 9f 02'): '8c 24 9f 02',
            B('9c 01 9f 37'): '9c 01 9f 37',
            B('33'): '33',
            B('9f 45'): '9f 45',
            B('03 8d'): '03 8d',
            B('08 9f 21'): '08 9f 21',
            B('0a 8a'): '0a 8a',
            }

    def __init__(self, otherself):
        if not self[0] == 0x70:
            raise PromisingDataException("First byte is assumed to be 0x70")
        if len(self) != int(self[1]) + 2:
            raise PromisingDataException("Wrong length")

        self.unparsed = {}

        self.__interpret_fields(self.__split_to_fields())

    def __split_to_fields(self):
        fields = {}

        remaining = self[2:]
        while remaining:
            for (field, label) in self.LABELS.items():
                if remaining[:len(field)] == field:
                    data_length = int(remaining[len(field)])
                    data = remaining[len(field)+1:len(field)+1+data_length]
                    if len(data) != data_length:
                        raise PromisingDataException("Wrong length")
                    remaining = remaining[len(field)+1+data_length:]
                    break
            else:
                raise PromisingDataException("Unknown field indicator, can't decide indicator length (remaining: %r)"%remaining)

            fields[label] = data
            if not any(remaining.intiter()):
                break # at times, fields end with zeros

        return fields

    def __interpret_fields(self, fields):
        if 'card number' in fields:
            self.card_number = fields.pop('card number').decode_bcd() # might just as well be int instead of bcd, but that would fail instead of silently reporting a wrong number for >=16

        if 'account number' in fields:
            data = fields.pop('account number')
            self.account_number = data[3:-1].decode_bcd()
            self.unparsed['account number (rest of field)'] = (data[:3], data[-1:])

        if 'expiry date' in fields:
            self.expiry_date = DateYYMMDD(fields.pop('expiry date')).date

        self.unparsed.update(fields)
