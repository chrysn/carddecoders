# encoding: utf-8

from __future__ import with_statement
from __future__ import absolute_import

import sys
import pkgutil
import operator
import datetime
import time
import os
import sys
import traceback

import gtk

from carddecoders.nonblocking_proxies import GCardFrameworkManager

from .. import quick
from . import addresses
from ... import conversation
from ... import replay

from carddecoders.gtktools import AboutDialogWithMarkup, safecb
from .guicomponents import ProgressStatusbar, QuickTransactionView, QuickIndicatorArea

_ = lambda x:x

class Application(object):
    def __init__(self):
        self._card = None

        builder = gtk.Builder()
        builder.add_from_string(pkgutil.get_data('carddecoders.en1546.quick', 'gui.xml'))

        self.win = builder.get_object('MainWindow')
        self.win.connect('destroy', gtk.main_quit)

        uimanager = builder.get_object('UIMan')
        accelgroup = uimanager.get_accel_group()
        self.win.add_accel_group(accelgroup)

        def connect(action, callback):
            builder.get_object(action).connect('activate', safecb(callback))
        connect('New', self.cb_new)
        connect('Open', self.cb_open)
        connect('Save', self.cb_save)
        connect('Clear', self.cb_clear)
        connect('Quit', gtk.main_quit)
        connect('About', self.cb_about)
        self.saveaction = builder.get_object('Save') # needed in _update_save_action

        self.list = builder.get_object('TransactionView')
        self.indicator_area = builder.get_object('IndicatorArea')
        self.statusbar = builder.get_object('StatusBar')

        self.win.show_all()

        self.cardprocess = GCardFrameworkManager(address=("127.0.0.1", 5000), authkey="foobar")
        self.cardprocess.connect()

        self._unsaved_connections = []

        self.load_blank()

    def has_unsaved_connections(self):
        return bool(self._unsaved_connections)

    def add_unsaved_connection(self, conn):
        conn.read_time = datetime.datetime.now() # needed for default filename when saving
        self._unsaved_connections.append(conn)

        self._update_save_action()

    def _update_save_action(self):
        self.saveaction.props.sensitive = self.has_unsaved_connections()

    def cb_new(self, *args):
        self.load_current()

    def cb_open(self, *args):
        chooser = gtk.FileChooserDialog(action=gtk.FILE_CHOOSER_ACTION_OPEN, buttons=(gtk.STOCK_CANCEL,gtk.RESPONSE_CANCEL,gtk.STOCK_OPEN,gtk.RESPONSE_OK))
        response = chooser.run()
        if response == gtk.RESPONSE_OK:
            file = chooser.get_filename()
        else:
            file = None
        chooser.destroy()
        if file:
            self.load_log(file)

    def cb_save(self, *args):
        assert self._unsaved_connections
        for f in self._unsaved_connections[:]:
            if self.save_connection(f):
                self._unsaved_connections.pop()
        self._update_save_action()

    def save_connection(self, con):
        """Offer to save a connection, return True if successfully saved and
        False if aborted."""

        chooser = gtk.FileChooserDialog(title=_("Choose a location to save the log"), action=gtk.FILE_CHOOSER_ACTION_SAVE, buttons=(gtk.STOCK_CANCEL,gtk.RESPONSE_CANCEL,gtk.STOCK_SAVE,gtk.RESPONSE_OK))
        chooser.set_current_name('%s.log'%con.read_time.strftime('%Y-%m-%d_%H-%M-%S'))
        chooser.set_do_overwrite_confirmation(True)
        response = chooser.run()
        if response == gtk.RESPONSE_OK:
            filename = chooser.get_filename()
        else:
            filename = None
        chooser.destroy()
        if filename is not None:
            with open(filename, 'w') as f:
                f.write(con.conversation.serialize())
            return True
        else:
            return False

    def cb_clear(self, *args):
        self.load_blank()

    def cb_about(self, *args):
        ad = AboutDialogWithMarkup()
        ad.props.program_name = _("QuickGui")

        ad.set_comments_markup("%s\n%s"%(
                _("A viewer for card infos of the Austrian Quick payment system."),
                _('Powered by <a href="http://www.quintessenz.org/">quintessenz</a>.'),
                ))
        ad.props.copyright = _(u'© chrysn 2010, published under GPLv3+')
        ad.props.website = 'http://christian.amsuess.com/'
        ad.run()
        ad.destroy()

    def _set_card(self, card):
        self._card = card
        self.indicator_area.card = card

        model = gtk.ListStore(object, int, int, int, int) # first object contains all information, subsequent columns are only there for soring (nt_iep, delta, date, remaining)
        model.set_sort_column_id(1, gtk.SORT_DESCENDING) # default: sort by nt_iep
        self.list.set_model(model)
        self.ntiep2iter = {}
        card.connect('transaction-updated', safecb(self.cb_transaction_updated))

    def cb_transaction_updated(self, card, transaction_id, transaction):
        model = self.list.get_model()

        model_data = (
            transaction,
            transaction_id,
            transaction.delta.value if transaction.delta else 0,
            time.mktime(transaction.timestamp.timetuple()) if transaction.timestamp else 0, # given that i don't expect to ever see transactions conducted before epoch, transactions without time will hardly show up between timestamped ones -- and this is only used for sorting anyway
            transaction.remaining.value if hasattr(transaction, 'remaining') else -1, # e-purses usually don't allow you to borrow money
            )

        if transaction_id in self.ntiep2iter:
            iter = self.ntiep2iter[transaction_id]
            model.set(iter, *reduce(operator.add, zip(range(len(model_data)), model_data))) # what about a more python friendly interface, pygtk?
        else:
            iter = model.append(model_data)
            self.ntiep2iter[transaction_id] = iter

    card = property(lambda self: self._card, _set_card)

    def load_blank(self):
        self._unsaved_connections = []
        self._update_save_action()

        self.card = self.cardprocess.QuickCard()

    def load_current(self):
        dispatcher = self.cardprocess.ConnectionDispatcher()
        try:
            con = dispatcher.blocking_autodispatch(logging=True, signature=quick.QuickCard.signature)
        except Exception, e:
            print >>sys.stderr, e
            self.display_message(_('Selecting card failed'), _('The card could not be selected. Please check if one card reader is connected and a card is inserted.'), gtk.MESSAGE_ERROR)
            return

        self.add_unsaved_connection(con)

        self._load_connection(con)

    def _load_connection(self, con):
        def progresshandler(card, progress):
            self.statusbar.progressbar.set_fraction(progress)
        def progresscompleted():
            self.statusbar.progressbar.props.visible = False
        def cardreaderror(error):
            if isinstance(error, quick.DifferentCard):
                d = gtk.MessageDialog(type=gtk.MESSAGE_QUESTION)
                d.props.text = _('Card IDs differ')
                d.props.secondary_text = _("The card being read is different from the card data currently shown. Data from different cards can't be shown in one view.\n\nDo you want to clear the current data and proceed reading the card?")
                d.add_button(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL)
                d.add_button(gtk.STOCK_CLEAR, gtk.RESPONSE_YES)
                response = d.run()
                d.destroy()
                if response == gtk.RESPONSE_YES:
                    self.load_blank()
                    self.card.connect('progress', safecb(progresshandler))
                    self.card.add_connection(con, safecb(progresscompleted), safecb(cardreaderror))
                else:
                    progresscompleted()
            else:
                self.display_message(_('Error reading card'), _('There was an unexpected error reading the card, error message:')+'\n\n%s'%error, gtk.MESSAGE_ERROR)
        self.statusbar.progressbar.props.visible = True
        self.statusbar.progressbar.set_fraction(0)
        self.card.connect('progress', safecb(progresshandler))
        self.card.add_connection(con, safecb(progresscompleted), safecb(cardreaderror))

    def load_log(self, file):
        data = open(file).read()
        conv = conversation.Conversation.deserialize(data)
        repl = replay.NaiveReplayConnection(conv)

        self._load_connection(repl)

    def display_message(self, text, secondary_text, type=gtk.MESSAGE_INFO):
        d = gtk.MessageDialog(type=type, buttons=gtk.BUTTONS_OK)
        d.props.text = text
        d.props.secondary_text = secondary_text
        d.run()
        d.destroy()

    def parse_commandline(self, argv):
        argv = argv[:]

        if '-n' not in argv:
            self.load_current()
        else:
            argv.remove('-n')
        if len(argv) >= 2:
            for f in argv[1:]:
                self.load_log(f)

    def run(self):
        gtk.main()
