"""Abstraction over the Austrian "Quick" IEP card"""

from __future__ import division

from ...bytestring import ByteString as B
from ...iso7816.apdu import SelectFile, ReadBinary, ReadRecord
from ...iso7816.sw import CardException, CardException6A83, CardException6A00

from ...replay import ReplayError

from ... import signatures

from .. import SpecViolation, PromisingDataException

from . import fields

class DifferentCard(Exception):
    """Raised when trying to feed an QuickCard object a connection to a
    different card"""

class QuickCard(object):
    """Austrian "Quick" IEP card decoder. Takes a list of connections
    (typically at most one of them an online card, the rest from logs; the list
    has to be sorted from oldest to newest) and stores not only the state but
    the history of a card."""

    signature = signatures.SmartcardSignature([
        '3B BF 11 00 81 31 .. 45 45 50 41 00 00 00 00 .. .. .. .. 00 00 .. .. ..'
        ],
        ['Austrian "quick" e-purse']
        )

    def __init__(self, connections=(), progress_callback=lambda connection, progress:None):
        self.transactions = {}
        self.card_id = None # unique number of this quick card
        self.current_money = None
        self.routingcode = None
        self.limit = None
        self.expiry_date = None
        self.account_number = None
        self.card_number = None # increments for every card issued for this bank account, starting at 1

        # must always match by index
        self.unparsed = []
        self.connections = []

        self._read_connections(connections, progress_callback)

    def _set_static_property(self, name, value):
        """Set a property of the object, but don't overwrite pre-existing
        values and rather raise an Exception."""

        if getattr(self, name) is None:
            setattr(self, name, value)
        else:
            if getattr(self, name) != value:
                if name == 'card_id':
                    raise DifferentCard() # special treatment so user interfaces can easily catch this typical case
                else:
                    raise PromisingDataException("Property changed: %s"%name)

    def _read_connections(self, connections, progress_callback=lambda connection, progress:None):
        for c in connections:
            self.add_connection(c, progress_callback=lambda progress: progress_callback(c, progress))

    def add_connection(self, connection, progress_callback=lambda progress:None):
        """Read information from a new connection. Adding a connection with a
        card that can not be read (not a quick card or wrong card id) does not
        leave the QuickCard object in an unusable state."""
        c = connection
        PROGRESS_SUM = (1 + # ATR
                3 + # readBinary calls
                2 + # df09 stuff
                8 + # LoadRecord
                16 + # PurchaseRecord
                2*16 + # pin purchase
                2*16 + # pin cash record
                32 + # more records
                32 + # other more records
                0)
        progress_done = [0] # storing this in a list is a hack to let a function increment it
        def progress_update(increment):
            progress_done[0] = progress_done[0] + increment
            progress_callback(progress_done[0]/PROGRESS_SUM)

        connection_unparsed = {}


        atr = c.getATR()

        if not self.signature.match_atr(atr):
            raise Exception("Not a quick card") # FIXME: exception type

        atr = fields.CardATR(atr)

        self._set_static_property('card_id', atr.card_id)

        connection_unparsed['atr'] = atr.unparsed

        progress_update(1)


        c.selectFile((0x3f, 0x00))
        c.selectFile((0x00, 0x02))
        # names used here (data1 etc) result from project history and don't have a particular meaning
        data1 = fields.File_3f00_0002(c.readBinary())

        self._set_static_property('routingcode', data1.routingcode)
        self._set_static_property('expiry_date', data1.expiry_date.date)
        connection_unparsed['data1'] = data1.unparsed

        progress_update(1)


        c.selectFile((0xdf, 0x01))

        c.selectFile((0x01, 0x01))
        data2 = fields.File_df01_0101(c.readBinary())

        self._set_static_property('limit', data2.limit)
        self._set_static_property('expiry_date', data2.expiry_date.date)
        connection_unparsed['data2'] = data2.unparsed

        progress_update(1)


        c.selectFile((0x01, 0x02))
        data4 = fields.File_df01_0102(c.readBinary())

        self.current_money = fields.decode_money(data4, data2[21:23])

        progress_update(1)


        try:
            c.selectFile((0xdf, 0x09))
            c.selectFile((0x00, 0x01))
        except (CardException, ReplayError):
            progress_update(2)
        else:
            df09_0001_r1 = fields.FieldBasedData_df09_0001(c.readRecord(1))
            connection_unparsed['df09 0001 record 1'] = df09_0001_r1.unparsed
            self._set_static_property('account_number', df09_0001_r1.account_number)
            self._set_static_property('card_number', df09_0001_r1.card_number)
            self._set_static_property('expiry_date', df09_0001_r1.expiry_date)
            progress_update(1)

            df09_0001_r2 = fields.FieldBasedData_df09_0001(c.readRecord(2))
            connection_unparsed['df09 0001 record 2'] = df09_0001_r2.unparsed
            progress_update(1)



        per_file_ntiep = [] # if files that are used by the card are missing, this may show up as missing nt_iep numbers

        def read_file(preparation_commands, record_range, record_interpretation, record2transaction, pass_on_notfound=False, recordfilter=lambda raw: any(raw.intiter()), response_handlers=None, pass_on_recordnotfound=False):
            response_handlers = response_handlers or [None] * len(preparation_commands)
            try:
                for (i, (command, response_handler)) in enumerate(zip(preparation_commands, response_handlers)):
                    response = c.check_transmit(command)

                    if response_handler is not None:
                        response_handler(response)
                    else:
                        if response:
                            connection_unparsed["%s response %d"%(record2transaction.__name__, i)] = response
            except (CardException, ReplayError): # ReplayError: accept old dumps from times when these records were not yet requested
                if pass_on_notfound:
                    progress_update(len(record_range))
                else:
                    raise
            else:
                file_ntiep = []

                for record_id in record_range:
                    try:
                        raw_record = c.readRecord(record_id)
                    except CardException6A83: # record not found
                        if pass_on_notfound:
                            continue
                        else:
                            raise
                    except CardException6A00: # "no information provided"; happens with older cards
                        continue # there seems not to be anything
                    finally:
                        progress_update(1)

                    if not recordfilter(raw_record):
                        continue
                    meaningful_record = record_interpretation(raw_record)
                    transaction = record2transaction(meaningful_record)
                    self._update_or_insert_record(transaction)

                    file_ntiep.append(transaction.nt_iep)

                per_file_ntiep.append(file_ntiep)

        c.selectFile((0xdf, 0x01))

        read_file(
                [SelectFile((0x01, 0x03))],
                range(1, 9),
                fields.LoadRecord,
                LoadTransaction
                )

        read_file(
                [SelectFile((0x01, 0x04))],
                range(1, 17),
                fields.PurchaseRecord,
                PurchaseTransaction
                )

        read_file(
                [SelectFile((0xd0, 0x40, 0x00, 0x00, 0x03, 0x00, 0x00, 0x02), direct_df=True), SelectFile((0x00, 0x03))],
                range(1, 17),
                fields.Record_df01_applet34_0003,
                PinPurchaseTransaction,
                True # fails on pure quick cards
                )
        read_file(
                [SelectFile((0xdf, 0x03)), SelectFile((0x00, 0x03))],
                range(1, 17),
                fields.Record_df01_applet34_0003,
                PinPurchaseTransaction,
                True # fails on pure quick cards
                )

        read_file(
                [SelectFile((0xd0, 0x40, 0x00, 0x00, 0x04, 0x00, 0x00, 0x02), direct_df=True), SelectFile((0x00, 0x03))],
                range(1, 17),
                fields.Record_df01_applet34_0003,
                PinCashTransaction,
                True # fails on pure quick cards
                )
        read_file(
                [SelectFile((0xdf, 0x04)), SelectFile((0x00, 0x03))],
                range(1, 17),
                fields.Record_df01_applet34_0003,
                PinCashTransaction,
                True # fails on pure quick cards
                )

        def parse_df_response(response):
            response = fields.Response_dfiep(response)
            # what should i do with that?

        # more records, meaning not yet known exactly
        # which kind of record is used is decided by length
        unknown_record_by_length = dict((x.LENGTH, x) for x in
                [fields.Record_df01_applet19_001d, fields.Record_df01_applet34_0003, fields.Record_df09_001d]
                )
        unknown_record_interpretation = lambda raw: unknown_record_by_length[len(raw)](raw)

        read_file(
                [SelectFile((0xd0, 0x40, 0x00, 0x00, 0x19, 0x00, 0x02), direct_df=True), SelectFile((0x00, 0x1d))],
                range(1, 33),
                unknown_record_interpretation,
                UnknownTransaction,
                True,
                lambda raw: any(raw.intiter()) and any(raw[2:51].intiter()), # [2:51]: seen; has transaction number but i have no idea what this could possibly mean
                response_handlers = [parse_df_response, None],
                pass_on_recordnotfound=True,
                )

        read_file(
                [SelectFile((0xdf, 0x09)), SelectFile((0x00, 0x1d))],
                range(1, 33),
                unknown_record_interpretation,
                UnknownTransaction,
                True,
                response_handlers = [parse_df_response, None]
                )


        if any(per_file_ntiep):
            all_ntiep_start = max(min(x) for x in per_file_ntiep if x) # starting from there, we should have all iep until...
            all_ntiep_end = max(max(x) for x in per_file_ntiep if x) # ... here

            known_ntiep = set(sum(per_file_ntiep, []))
            should_be_known_ntiep = set(range(all_ntiep_start, all_ntiep_end+1))
            missing_ntiep = should_be_known_ntiep - known_ntiep

            if missing_ntiep:
                connection_unparsed['missing nt_iep'] = sorted(missing_ntiep)

        assert progress_done[0] == PROGRESS_SUM, "Expected %d operations, but got %d."%(PROGRESS_SUM, progress_done[0])

        self.connections.append(c)
        self.unparsed.append(connection_unparsed)

    def _update_or_insert_record(self, record):
        if record.nt_iep in self.transactions:
            self.transactions[record.nt_iep].update(record)
        else:
            self.transactions[record.nt_iep] = record

class Transaction(object):
    """Objects describing a transaction on a Quick card.

    Over time, transactions are described by up to one record per connection /
    dump and should not change in their essential parts (_static_properties),
    but they sometimes vary in not yet parsed data (_variable_properties)."""

    def __init__(self, **kwargs):
        for key in self._static_properties:
            setattr(self, key, kwargs[key])
        for key in self._variable_properties:
            setattr(self, key, [kwargs[key]])

    def update(self, other):
        """Compare record with another connection's / dump's version of the
        record, and update the variable properties"""

        if type(self) != type(self):
            raise PromisingDataException("NT_IEP is used for different record types.")

        for key in self._static_properties:
            if getattr(self, key) != getattr(other, key):
                raise PromisingDataException("Attribute '%s' changed."%key)
        for key in self._variable_properties:
            getattr(self, key).extend(getattr(other, key))

class LoadTransaction(Transaction):
    _static_properties = ('nt_iep', 'delta', 'id_psam', 'timestamp', 'remaining',)
    _variable_properties = ('unparsed',)

    def __init__(self, record):
        super(LoadTransaction, self).__init__(
                nt_iep=record.nt_iep,
                delta=record.delta,
                id_psam=record.id_psam,
                timestamp=record.timestamp,
                remaining=record.remaining,
                unparsed=record.unparsed
                )

class PurchaseTransaction(Transaction):
    _static_properties = ('nt_iep', 'delta', 'id_psam', 'nt_psam', 'timestamp', 'remaining',)
    _variable_properties = ('unparsed',)

    def __init__(self, record):
        super(PurchaseTransaction, self).__init__(
                nt_iep=record.nt_iep,
                delta=record.delta,
                id_psam=record.id_psam,
                nt_psam=record.nt_psam,
                timestamp=record.timestamp,
                remaining=record.remaining,
                unparsed=record.unparsed
                )

class PinPurchaseTransaction(Transaction):
    _static_properties = ('nt_iep', 'delta', 'text', 'timestamp',)
    _variable_properties = ('unparsed',)

    def __init__(self, record):
        super(PinPurchaseTransaction, self).__init__(
                nt_iep=record.nt_iep,
                delta=record.delta,
                timestamp=record.timestamp,
                text=record.text,
                unparsed=record.unparsed,
                )

class PinCashTransaction(Transaction):
    _static_properties = ('nt_iep', 'delta', 'text', 'timestamp',)
    _variable_properties = ('unparsed',)

    def __init__(self, record):
        super(PinCashTransaction, self).__init__(
                nt_iep=record.nt_iep,
                delta=record.delta,
                timestamp=record.timestamp,
                text=record.text,
                unparsed=record.unparsed,
                )

class UnknownTransaction(Transaction):
    """Mainly for testing as long as format is not fixed"""
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

    def __init__(self, record):
        d = dict((k, getattr(record, k)) for k in dir(record) if not k.startswith('_') and isinstance(getattr(type(record), k), property))
        self._static_properties = d.keys()
        super(UnknownTransaction, self).__init__(**d)

    _variable_properties = () # assume nothing is variable
