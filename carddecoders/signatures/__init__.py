"""Infrastructure to handle card signatures as used in smartcard_list.txt of pcsc-tools"""
# found out that a similar feaure is available in pyscard's CardConnection; might be mergable if desired

import os
import re

from ..bytestring import ByteString

class SmartcardList(object):
    """Reader for files in the style of smartcard_list.txt.

    To use the shipped list, use this module's known_copy instance.

    >>> known_cards['3F 7F 13 25 03 38 B0 04 FF FF 4A 50 00 00 29 48 55 01 23 45']
    <SmartCardSignature for u'DSS/DTV HU'>
    """
    def __init__(self, file=None):
        self.signatures = []
        if file is not None:
            self.load(file)

    def load(self, file):
        records = []

        current = {}

        def flush():
            if not current:
                return
            if 'atr' not in current:
                raise ValueError('Incomplete entry in list file.')
            records.append((current['atr'], current.get('description', [])))
            current.clear()

        for line in file:
            if '#' in line:
                line = line[:line.index('#')]
            line = line.rstrip()

            if not line:
                flush()
            elif line.startswith('\t'):
                current.setdefault('description', []).append(line[1:].decode('utf8'))
            else:
                assert 'atr' not in current
                current['atr'] = line
        flush()

        grouped = {}
        for atr, description in records:
            grouped.setdefault(tuple(description), []).append(atr)

        for description, atrs in grouped.items():
            self.signatures.append(SmartcardSignature(atrs, description))

    def __getitem__(self, atr):
        notwhatiwant = object()
        result = self.get(atr, default=notwhatiwant)
        if result is notwhatiwant:
            raise KeyError("No matching card.")
        else:
            return result

    def get(self, atr, default=None):
        ret = None
        for s in self.signatures:
            if s.match_atr(atr):
                if ret is not None:
                    raise ValueError("Duplicate in list, matching %r"%atr)
                ret = s
        if ret is None:
            return default
        else:
            return ret

class SmartcardSignature(object):
    """Smartcard recognizer

    Stores all information available about a type of smart cards."""

    def __init__(self, atrs, description):
        self.atrs = atrs
        self.atrs_re = map(re.compile, atrs)
        self.description = description

    def match_atr(self, atr):
        if isinstance(atr, ByteString):
            atr = atr.plainhex().upper()
        return any(atr_re.match(atr) for atr_re in self.atrs_re)

    def __repr__(self):
        return '<SmartCardSignature for %s%s>'%(repr(self.description[0]) if self.description else 'card without description', '...' if len(self.description) > 1 else '')


smartcard_list = '/usr/share/pcsc/smartcard_list.txt'
if os.path.exists(smartcard_list):
    known_cards = SmartcardList(open(smartcard_list))
else:
    print "Smart card list could not be loaded" # FIXME
    known_cards = SmartcardList()
