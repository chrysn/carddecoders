from carddecoders.nonblocking_proxies import GCardFrameworkManager

manager = GCardFrameworkManager(address=("127.0.0.1", 5000), authkey="foobar")
server = manager.get_server()
server.serve_forever()
