"""multiprocessing.managers for nonblocking operation with GTK (or other main
loops, if someone implements them)

For synchronous multiprocessing.managers communication, you use proxies like
this:

>>> class MyObjectProxy(multiprocessing.managers.BaseProxy):
...     def do_stuff(self, *args, **kwargs):
...         return self._callmethod('do_stuff', args, kwargs)

(Most times, a proxy of this kind is created implicitly with all `exposed`
methods.)

For objects to be used in an asynchronous way inside a loop, proxies look like
this:

>>> class MyObjectProxy(AsyncBaseProxy):
...     def do_stuff_asynchronously(self, on_success, on_error, *args, **kwargs):<
...         self._async_callmethod('do_stuff', args, kwargs, on_success, on_error)

Calling a MyObjectProxy's do_stuff_asynchronously method will start the
function call in the manager's process and will call on_success(result) or
on_error(exception) as soon as the result is ready and the main loop runs idle.

AsyncBaseProxy objects require to be managed by an `AsyncManager`
implementation instead of a regular `BaseManager` as the manager is responsible
for queueing and distributing the various requests. Currently, the only
implementation is `GAsyncManager` which uses the gobject main loop to wait for
events.

You can still use the synchronous methods for blocking calls, but have to make
sure the proxy derives from AsyncBaseProxy as the _callmethod has been
overwritten to allow previously requested results to return to their callback
instead of crosstalking into the blocking call.

When using generators, `AsyncBaseProxy` objects provide a
`_semisync_callgenerator` method that will make the generator call yield to a
given function:

>>> class MyObjectProxy(AsyncBaseProxy):
    def do_much(self, on_yield, on_end, on_error, *args, **kwargs):
        self._semisync_callgenerator('do_much', args, kwargs, on_yield, on_end, on_error)
"""

import select

import multiprocessing
import multiprocessing.managers

import weakref

import gobject

class AsyncManager(multiprocessing.managers.BaseManager):
    """Manager that supports queueing of requests (with BaseManager, it is
    common to block waiting for a request, so there can never be a queue of
    requests).

    Queueing has to be managed on the side of the manager (as opposed to being
    managed by the proxies on their own) as they don't have a way of knowing
    which request a response is belonging to."""

    # IMPORTANT: every time you send to the connection, you append to the
    # callback queue. everytime you receive from the connection, you pop from
    # the callback queue.

    # FIXME: prevent non-AsyncBaseProxy objects from being generated or overwrite
    # their _callmethod from AsyncBaseProxy

    def __init__(self, *args, **kwargs):
        super(AsyncManager, self).__init__(*args, **kwargs)

        self._callback_queue = []

    def queue_request(self, request_message, response_handler):
        raise NotImplementedError("To issue request, select a main loop handler by choosing an appropriate subclass, like `GAsycnManager`.")

    def queue_request_and_block(self, request_message):
        # FIXME: most of this could be handled in the generic AsyncManager
        # instead of the gobject GAsycnManager, but unsetting the __watch is in
        # the middle of the code. should be generalized as soon as other
        # backends are added.
        raise NotImplementedError("To issue request, select a main loop handler by choosing an appropriate subclass, like `GAsycnManager`.")

    def _get_conn(self):
        tls, idset = multiprocessing.managers.BaseProxy._address_to_local[self._address] # i'm not sure how this is created, but whoever makes the request will hopefully already have taken care of that.
        return tls.connection

    def _receive_and_react(self):
        conn = self._get_conn()
        response = conn.recv()
        callback = self._callback_queue.pop(0)
        callback(response)

class GAsyncManager(AsyncManager):
    """Implementation of AsyncManager using the gobject main loop."""
    def __init__(self, *args, **kwargs):
        super(GAsyncManager, self).__init__(*args, **kwargs)

        self.__watch = None # handle of the gobject io watch listening for input. will only be set if there are pending callbacks and will keep self referenced from the mainloop

        self.__sync_responses = {}

    def queue_request(self, request_message, response_handler):
        conn = self._get_conn()

        if self.__watch is None:
            self.__watch = gobject.io_add_watch(conn, gobject.IO_IN, lambda source, condition: (self.__on_selectable(), True)[1])

        self._callback_queue.append(response_handler)
        conn.send(request_message)

    def queue_request_and_block(self, request_message):
        conn = self._get_conn()

        # this is a bit complicated as it is possible that while working off
        # pending calls, another _and_block call is issued.
        #
        # the outer blocking call's response will be worked off in the inner
        # blocking loop, but can't be returned immediately as it has to finish
        # its own work first.
        #
        # the results are thus stored in the manager's __sync_responses
        # dictionary, where each stacked _and_block call looks for it, and it
        # simply works off pending requests until its own is set.

        own_id_creator = object() # easiest way i can think of to create at least per-process unique numbers
        own_id = id(own_id_creator)

        def received_own(result):
            self.__sync_responses[own_id] = result
        self._callback_queue.append(received_own)
        conn.send(request_message)

        while own_id not in self.__sync_responses:
            self._receive_and_react()

        if not self._callback_queue and self.__watch:
            gobject.source_remove(self.__watch)
            self.__watch = None

        return self.__sync_responses.pop(own_id)

    def __on_selectable(self):
        conn = self._get_conn()

        while not conn.closed and conn.poll():
            self._receive_and_react()

        if not self._callback_queue:
            gobject.source_remove(self.__watch)
            self.__watch = None

class AsyncBaseProxy(multiprocessing.managers.BaseProxy):
    def __init__(self, *args, **kwargs):
        super(AsyncBaseProxy, self).__init__(*args, **kwargs)

        if not isinstance(self._manager, AsyncManager): # FIXME: check for queue attribute
            raise Exception("Can't use a AsyncBaseProxy without an AsyncManager.")

    def __callmethod1(self, methodname, args, kwds):
        # copied from BaseProxy._callmethod and split before the recv().
        # equivalent _callmethod is
        # def _callmethod(self, methodname, args, kwds):
        #     request = self.__callmethod1(methodname, args, kwds)
        #     conn = self._tls.connection
        #     conn.send(request)
        #     kind, result = conn.recv()
        #     return self.__callmethod2(kind, result)

        from multiprocessing import util
        import threading
        # copied starts here
        try:
            conn = self._tls.connection
        except AttributeError:
            util.debug('thread %r does not own a connection',
                       threading.current_thread().name)
            self._connect()
            conn = self._tls.connection

        # copied ends here
        return (self._id, methodname, args, kwds)

    def __callmethod2(self, kind, result):
        # this is the second part of the copied code fragment starting in __callmethod1

        from multiprocessing.managers import convert_to_error, dispatch
        conn = self._tls.connection # already prepared in 1
        # copied starts here
        if kind == '#RETURN':
            return result
        elif kind == '#PROXY':
            exposed, token = result
            proxytype = self._manager._registry[token.typeid][-1]
            proxy = proxytype(
                token, self._serializer, manager=self._manager,
                authkey=self._authkey, exposed=exposed
                )
            conn = self._Client(token.address, authkey=self._authkey)
            dispatch(conn, None, 'decref', (token.id,))
            return proxy
        raise convert_to_error(kind, result)
        # copied ends here

    def _async_callmethod(self, methodname, args, kwds, on_success, on_error):
        """Start execution of a method, call back on success or error"""

        request = self.__callmethod1(methodname, args, kwds)
        def callback(data):
            kind, result = data
            try:
                result = self.__callmethod2(kind, result)
            except BaseException, e:
                on_error(e)
            else:
                on_success(result)

        self._manager.queue_request(request, callback)

    def _callmethod(self, methodname, args=(), kwds={}):
        # don't call and block, for there might be other objects in the queue
        # that will return earlier.
        #
        # instead, instruct the manager to finish pending stuff while blocking
        # for my results.

        request = self.__callmethod1(methodname, args, kwds)
        kind, result = self._manager.queue_request_and_block(request)
        return self.__callmethod2(kind, result)

    def _semisync_callgenerator(self, methodname, args, kwds, on_yield, on_end, on_error):
        """Call and exhaust a generator function in the referent's process,
        call back on every yield.

        Beware that the calls to the generator's next iteration will be issued
        when the the previous has ended. By that time, calls to the main object
        might already have queued up.

        This is called semisync as completely asynchronous generator calls
        would generate a handle that can be told to start processing the next
        iteration and call back when the result is ready. This would enable
        passing results back in."""

        if methodname not in self._method_to_typeid_:
            # this assumes everyone who writes an explicit proxy (as necessary
            # when using AsyncBaseProxy) also sets the _method_to_typeid_ in
            # the class and not when registering it.
            #
            # FIXME: could also check if it really maps to a AsyncGeneratorProxy
            raise Exception("Calling a generator without a method_to_typeid creating a proxy will fail.")

        def on_generator_created(generator):
            generator.exhaust_to(on_yield, on_end, on_error)

        self._async_callmethod(methodname, args, kwds, on_generator_created, on_error) # can't create the generator in a blocking way because generator creation will be instantaneous, but there might be earlier calls that block.


class AsyncGeneratorProxy(multiprocessing.managers.IteratorProxy, AsyncBaseProxy):
    def async_next(self, on_success, on_error):
        self._async_callmethod('next', (), {}, on_success, on_error)

    def exhaust_to(self, on_yield, on_end, on_error):
        """Call on_yield as long as the generator produces data, request more
        when new data arrives, and call on_end or on_error if the generator
        terminates or fails.

        See AsyncBaseProxy._semisync_callgenerator for more details."""

        # when called like this, this object is prone to stick around even
        # after the StopIteration, although it could be gc'd reference-wise.
        # call gc.collect() manually if you want it to be cleaned up, can't do
        # more than make sure it does not stick around anywhere.
        #
        # i should add that my understanding of python gc and refcount logics
        # are very limited and i could easily be wrong here.

        def successful_next(result):
            on_yield(result)

            next_step()

        def unsuccessful_next(exception):
            if isinstance(exception, StopIteration):
                on_end()
            else:
                on_error(exception)

        def next_step():
            self.async_next(successful_next, unsuccessful_next)

        next_step()
