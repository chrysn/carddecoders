"""Hack to get events into multiprocessing managers

To use this, use a manager inheriting from `EventSupportingAsyncManager` (in a
gobject main loop that would be `GEventSupportingAsyncManager`) and have your
proxies inherit from EventPassingProxy.

The referents can emit an Event by passing it to the method passed to them on
their set_event_processor method.

Concerning proxies, you can go with or without gobject:

* GEventPassingProxy will wire up all gobject signals defined for the proxy
  class to the respectively named events; the event's payload will be passed to
  the emit method as *args, so it has to be a tuple.
* EventPassingProxy will accept add_event_handler(event_name, callback)
  instructions.

Neither provides a method to unregister yet.""" # FIXME

import weakref

from . import AsyncManager, GAsyncManager, AsyncBaseProxy
import multiprocessing.managers

import gobject

class Event(object):
    def __init__(self, name, payload=None):
        self.name = name
        self.payload = payload

class EventSupportingServer(multiprocessing.managers.Server):
    def create(self, c, typeid, *args, **kwargs):
        ident, exposed = super(EventSupportingServer, self).create(c, typeid, *args, **kwargs)
        obj = self.id_to_obj[ident][0]
        if hasattr(obj, 'set_event_processor'):
            # these two weakrefs are mainly a precaution as i'm not sure when they would make a difference, but i prefer None's AttributeErrors to leaking objects
            weakself = weakref.ref(self)
            weakobj = weakref.ref(obj)
            obj.set_event_processor(lambda event: weakself().__send_event(weakobj(), typeid, event))
        return ident, exposed

    def __send_event(self, obj, typeid, event):
        event.source_id = "%x"%id(obj) # wrapping id in hex seems to be the way of multiprocessing
        self.__conn().send(('#EVENT', event))

    def serve_client(self, conn):
        self.__conn = weakref.ref(conn)
        try:
            return super(EventSupportingServer, self).serve_client(conn)
        finally:
            del self.__conn

class EventPassingProxy(AsyncBaseProxy):
    def add_event_handler(self, event_name, callback):
        referent_id = self._token.id
        self._manager._event_callbacks.setdefault((referent_id, event_name), []).append(callback)

class GEventPassingProxy(AsyncBaseProxy):
    """Proxy that sends incoming events out as Gobject signals."""
    def __init__(self, *args, **kwargs):
        super(GEventPassingProxy, self).__init__(*args, **kwargs)

        referent_id = self._token.id
        for event_name in gobject.signal_list_names(self):
            self._manager._event_callbacks.setdefault((referent_id, event_name), []).append(lambda event, event_name=event_name: self.emit(event_name, *event.payload))

class EventSupportingAsyncManager(AsyncManager):
    _Server = EventSupportingServer
    def get_server(self):
        # copy/pasted, and replaced Server with cls._Server -- why? (FIXME) -- issue 10850
        assert self._state.value == multiprocessing.managers.State.INITIAL
        return self._Server(self._registry, self._address,
                self._authkey, self._serializer)

    def __init__(self, *args, **kwargs):
        super(EventSupportingAsyncManager, self).__init__(*args, **kwargs)

        self._event_callbacks = {}

    def _receive_and_react(self):
        # explicitly overwrite AsyncManager's receive_and_react, as in context
        # of events, not all recv() messages trigger popping the queue
        conn = self._get_conn()
        response = conn.recv()
        if response[0] == '#EVENT':
            self._handle_event(response[1])
            return
        callback = self._callback_queue.pop(0)
        callback(response)

    def _handle_event(self, event):
        key = (event.source_id, event.name)
        if key in self._event_callbacks:
            for callback in self._event_callbacks[key]:
                callback(event)


class GEventSupportingAsyncManager(EventSupportingAsyncManager, GAsyncManager):
    pass
